<?php 
	require_once('inc/config.inc');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Form Test</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/styles.css">
</head>
<body>
<div class="container">
	<div class="form-group">
		<legend>Washingtonian Test Form</legend>
	</div>
	<div class="container">
	<form role="form" id="form" class="form-horizontal" method="post" action="<?php print $form_action_location;?>">
		<input type="hidden" name="promotionkey" value="<?php print $promotionKey['holiday']['a']; ?>"/>
		<input type="hidden" name="diff_recipient" value="1">
		<input type="hidden" name="country" value="United States">
		<div class="row" id="mail">
			<h3>THIS IS FOR:</h3>
			<div class="row hidden">
				<div class="col-xs-12 form-error">
					<div class="col-sm-6">
						<div id="firstname"></div>
						<div id="lastname"></div>
						<div id="address1"></div>
						<div id="address2"></div>
					</div>
					<div class="col-sm-6">
						<div id="city"></div>
						<div id="state"></div>
						<div id="zip"></div>
						<div id="email"></div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label for="firstname" class="col-xs-5 control-label col-xs-pull-7">First Name:</label>
					<div class="col-xs-7 col-xs-push-5">
						<input type="text" class="form-control" id="firstname" name="firstname[]" maxlength="27">
					</div>
				</div>
				<div class="form-group">
					<label for="lastname" class="col-xs-5 control-label">Last Name:</label>
					<div class="col-xs-7">
						<input type="text" class="form-control" name="lastname[]" maxlength="27">
					</div>
				</div>
				<div class="form-group">
					<label for="address1" class="col-xs-5 control-label">Address 1:</label>
					<div class="col-xs-7">
						<input type="text" class="form-control" name="address1[]" maxlength="27">
					</div>
				</div>
				<div class="form-group">
					<label for="address2" class="col-xs-5 control-label">Address 2:</label>
					<div class="col-xs-7">
						<input type="text" class="form-control" name="address2[]" maxlength="27">
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="form-group">
					<label for="city" class="col-xs-5 control-label">City:</label>
					<div class="col-xs-7">
						<input type="text" class="form-control" name="city[]" maxlength="13">
					</div>
				</div>
				<div class="form-group">
					<label for="state" class="col-xs-5 control-label">State:</label>
					<div class="col-xs-7">
						<select class="form-control" name="state[]">
							<option value="" selected="selected">Select A State</option>
							<option value="AA" >APO/FPO-Americas</option>
							<option value="AE" >APO/FPO-Middle East</option>
							<option value="AP" >APO/FPO-Pacific</option>
							<option value="AK" >Alaska</option>
							<option value="AL" >Alabama</option>
							<option value="AR" >Arkansas</option>
							<option value="AS" >American Samoa</option>
							<option value="AZ" >Arizona</option>
							<option value="CA" >California</option>
							<option value="CO" >Colorado</option>
							<option value="CT" >Connecticut</option>
							<option value="DC" >District of Columbia</option>
							<option value="DE" >Delaware</option>
							<option value="FL" >Florida</option>
							<option value="GA" >Georgia</option>
							<option value="GU" >Guam</option>
							<option value="HI" >Hawaii</option>
							<option value="IA" >Iowa</option>
							<option value="ID" >Idaho</option>
							<option value="IL" >Illinois</option>
							<option value="IN" >Indiana</option>
							<option value="KS" >Kansas</option>
							<option value="KY" >Kentucky</option>
							<option value="LA" >Louisiana</option>
							<option value="MA" >Massachusetts</option>
							<option value="MD" >Maryland</option>
							<option value="ME" >Maine</option>
							<option value="MI" >Michigan</option>
							<option value="MN" >Minnesota</option>
							<option value="MO" >Missouri</option>
							<option value="MS" >Mississippi</option>
							<option value="MT" >Montana</option>
							<option value="NC" >North Carolina</option>
							<option value="ND" >North Dakota</option>
							<option value="NE" >Nebraska</option>
							<option value="NH" >New Hampshire</option>
							<option value="NJ" >New Jersey</option>
							<option value="NM" >New Mexico</option>
							<option value="NV" >Nevada</option>
							<option value="NY" >New York</option>
							<option value="OH" >Ohio</option>
							<option value="OK" >Oklahoma</option>
							<option value="OR" >Oregon</option>
							<option value="PA" >Pennsylvania</option>
							<option value="PR" >Puerto Rico</option>
							<option value="RI" >Rhode Island</option>
							<option value="SC" >South Carolina</option>
							<option value="SD" >South Dakota</option>
							<option value="TN" >Tennessee</option>
							<option value="TX" >Texas</option>
							<option value="UT" >Utah</option>
							<option value="VA" >Virginia</option>
							<option value="VT" >Vermont</option>
							<option value="WA" >Washington</option>
							<option value="WI" >Wisconsin</option>
							<option value="WV" >West Virginia</option>
							<option value="WY" >Wyoming</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="zip" class="col-xs-5 control-label">Zip Code:</label>
					<div class="col-xs-7">
						<input type="text" class="form-control" name="zip[]" maxlength="5">
					</div>
				</div>
				<div class="form-group">
					<label for="email" class="col-xs-5 control-label">Email:</label>
					<div class="col-xs-7">
						<input type="text" class="form-control" name="email[]" maxlength="50">
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<h3>CHOOSE YOUR EDTION</h3>
			<div class="row subs">
				<div class="col-sm-6">
					<div class="col-sm-12 sub radio">
						<label>
							<input type="radio" value="6" name="subscribe_offer">
							PRINT + DIGITAL ($14.99)</label>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="col-sm-12 sub radio">
						<label>
							<input type="radio" value="7" name="subscribe_offer">
							DIGITAL ONLY ($9.99)</label>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<h3>PAYMENT INFORMATION:</h3>
			<div class="col-xs-12">
				<div class="form-group">
					<label for="payment_method" class="col-xs-4 control-label">Payment Method:</label>
					<div class="col-xs-6">
						<select class="form-control" name="payment_method" id="cc_cards">
							<option value="" selected="selected">Select a payment method</option>
							<option value="mastercard">MasterCard</option>
							<option value="visa">Visa</option>
							<option value="amex">American Express</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="payment_cc" class="col-xs-4 control-label">Credit Card Number:</label>
					<div class="col-xs-6">
						<input type="text" class="form-control" name="payment_cc">
					</div>
				</div>
				<div class="form-group">
					<label class="col-xs-4 control-label">Expiration Date:</label>
					<div class="col-sm-2 col-xs-4">
						<select class="form-control" name="payment_exp_month">
							<option value="" selected="selected">Month</option>
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
						</select>
					</div>
					<div class="col-sm-2 col-xs-4">
						<select class="form-control" name="payment_exp_year">
							<option value="" selected="selected">Year</option>
							<option value="13" >2013</option>
							<option value="14" >2014</option>
							<option value="15" >2015</option>
							<option value="16" >2016</option>
							<option value="17" >2017</option>
							<option value="18" >2018</option>
							<option value="19" >2019</option>
							<option value="20" >2020</option>
							<option value="21" >2021</option>
							<option value="22" >2022</option>
							<option value="23" >2023</option>
						</select>
					</div>
				</div>
				<div class="row field">
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-xs-12 col-md-12 col-lg-12"></div>
		</div>
		<div class="form-group">
			<div class="col-xs-2 col-sm-offset-5">
				<button type="submit" name="submit" class="btn-lg btn-primary">Purchase</button>

			</div>
		</div>
	</form>
	<div class="footer center-block"> Created by Eric Sherred for <a target="_blank" href="http://www.washingtonian.com">Washingtonian Magazine</a></div>
</div>
<!-- Latest compiled and minified JS -->
<script src="//code.jquery.com/jquery.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
</body>
</html>