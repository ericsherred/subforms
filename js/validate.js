// Author: Eric Sherred
// For: Washingtonian Mag (www.washingtonian.com)
// Nov 7, 2013

// Works with jquery-validate.js: https://github.com/DiegoLopesLima/Validate
// Implementation:
// In <head> jquery.min.js & jquery-validate.min.js
// validate.js (this file) after the form to be validated
// <div class="form-error hidden"><div id="ONE_FOR_EACH_FIELD"></div></div>

// Inputs: 
// 		data-describedby="" - Use id from the error div that matches the field, i.e. First Name = firstname
// 		data-validate="" - Use validateExtend item from below that matteches necessary pattern, i.e. First Name = alpha
// 		data-description="" - Use description from below that matches the field, i.e. First Name = firstname
// 		data-required="true" - true makes the field required, false makes it not rquired
// Select/Radio:
// 		data-describedby="" - Use id from the error div that matches the field, i.e. First Name = firstname
// 		data-description=""- Use description from below that matches the field, i.e. First Name = firstname
// 		data-required="true" - true makes the field required, false makes it not rquired


	jQuery('form').validate({
		invalid : function() {
			$('.form-error').removeClass('hidden');
		},
		valid : function() {
			$('.form-error').addClass('hidden');
		},
		eachInvalidField : function() {
			//$(this).closest('li').addClass('error');
			//$(this).closest('span').addClass('error');
			$(this).addClass('error');
		},
		eachValidField : function() {
			$(this).closest('li').removeClass('error');
			$(this).closest('span').removeClass('error');
		},
		description : {
			firstname : {
				required : '<div class="error">First Name required</div>',
				pattern : '<div class="error">Please enter a valid First Name</div>',
				conditional : '<div class="error">Please enter a valid First Name</div>',
			},
			lastname : {
				required : '<div class="error">Last Name required</div>',
				pattern : '<div class="error">Please enter a valid Last Name</div>',
				conditional : '<div class="error">Please enter a valid Last Name</div>',
			},
			address1 : {
				required : '<div class="error">Street Address required</div>',
				pattern : '<div class="error">Please enter a valid Street Address</div>',
				conditional : '<div class="error">Please enter a valid Street Address</div>',
			},
			address2 : {
				required : '<div class="error">Street Addres Line 2 required</div>',
				pattern : '<div class="error">Please enter a valid Street Address Line 2</div>',
				conditional : '<div class="error">Please enter a valid Street Address Line 2</div>',
			},
			city : {
				required : '<div class="error">City required</div>',
				pattern : '<div class="error">Please enter a valid City</div>',
				conditional : '<div class="error">Please enter a valid City</div>',
			},
			state : {
				required : '<div class="error">State required</div>',
				pattern : '<div class="error">Please Select A Valid State</div>',
				conditional : '<div class="error">Please Select A Valid State</div>',
			},
			zip : {
				required : '<div class="error">Zip Code required</div>',
				pattern : '<div class="error">Please enter a valid Zip Code</div>',
				conditional : '<div class="error">Please enter a valid Zip Code</div>',
			},
			email : {
				required : '<div class="error">Email required</div>',
				pattern : '<div class="error">Please enter a valid Email</div>',
				conditional : '<div class="error">Please enter a validEmail</div>',
			},
			subscribe_offer : {
				required : '<div class="error">Subscription required</div>',
				pattern : '<div class="error">Please Select A Valid Subscription</div>',
				conditional : '<div class="error">Please Select A Valid Subscription</div>',
			},
			payment_method : {
				required : '<div class="error">Payment Method required</div>',
				pattern : '<div class="error">Please Select A Valid Payment Method</div>',
				conditional : '<div class="error">Please enter a valid Payment Method</div>',
			},
			payment_cc : {
				required : '<div class="error">Credt Card Number required</div>',
				pattern : '<div class="error">Please enter a valid Credit Card Number</div>',
				conditional : '<div class="error">Please enter a valid Credit Card Number</div>',
			},
			payment_exp_month : {
				required : '<div class="error">Credit Card Expiration Month required</div>',
				pattern : '<div class="error">Please Select A Valid Credit Card Expiration Mont</div>',
				conditional : '<div class="error">Please Select A Valid Credit Card Expiration Mont</div>',
			},
			payment_exp_year : {
				required : '<div class="error">Credit Card Expiration Year required</div>',
				pattern : '<div class="error">Please Select A Valid Credit Card Expiration Year</div>',
				conditional : '<div class="error">Please Select A Valid Credit Card Expiration Year</div>',
			},
		}
	});

	jQuery.validateExtend({
	    alpha : {
    	    pattern : /^[a-z ,.'-]+$/i
    	},
	    alphanumeric : {
    	    pattern : /^[a-z 0-9 #,.'-]+$/i
    	},
		email : {
			pattern : /^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/
		},
		zip : {
			pattern : /^\d{5}$/
		},
		cc : {
			pattern : /^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|6(?:011|5[0-9]{2})[0-9]{12}|(?:2131|1800|35\d{3})\d{11})$/
		}
	});
