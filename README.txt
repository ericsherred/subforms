/*	CDS-Subscriber Integration App	*/
/* 	Created by Mario Starks 		*/
/* 	Washingtonian.com   			*/
/* 	mstarks@washingtonian.com		*/
/*	Created August 7, 2013			*/
/*	******************************	*/

Contents of application structure: 

SUBSCRIBE folder contents: 
--------------------------------------

	... special
		This folder contains all custom designed A/B forms. Designs which deviate from the original control set.
		
	... control
		This folder contains all control/default designed forms. These were the forms originally in use before the integration of CDS/aMember (this app)
		
	... _includes
		This folder contains all classes, libraries, config files and server-side scripts
		
	... _form_processing.php
		This file hands and routes all logic and handling of subscriber input. 
		
	... ../_unusedassets
		Miscelleneous files here currently no longer in use, but archiving for easy retrieval just in case. 
	
