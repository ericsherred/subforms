<?php 
/* 	
	SPECIAL FORM A (custom layout/form-- WITH NO VARIATION as in control form-A). 
*/
	
require_once('../inc/config.inc');

?>

<!doctype html>
<html>
<head>
<meta charset="UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<link type="text/css" rel="stylesheet" href="../css/special-form-v3.css" />
<link type="text/css" rel="stylesheet" href="../css/form_error.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="../js/jquery-validate.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {

	$('#mailing-address').hide();
		
	$('#show-mailing').click(function() {		
		if( $('#show-mailing').is(':checked') ) {
			$('#mailing-address').fadeIn('fast');
			$('#sub-data > div.default').animate({'height':'630px'});
		}
	
		else {
			$('#mailing-address').fadeOut('fast');
			$('#sub-data > div.default').animate({'height':'300px'});
		}	
	});
	
	$('#sub-options input[type=radio]').click(function() {
		if( $('input[type=radio].all').is(':checked') ) {
			$(this).parent().parent().parent().addClass('picked');
			$(this).parent().parent().parent().siblings().removeClass('picked');
		}
		else if( $('input[type=radio].digital').is(':checked') ) {
			$(this).parent().parent().addClass('picked');
			$(this).parent().parent().siblings().removeClass('picked');
		}
	});

});

</script>
<title>Subscribe | Washingtonian</title>
</head>

<body>
<div id="wrapper">
<div id="inner-wrapper">
<header>

<div id="sub-header">
<img src="../images/cover/2014_Feb_small.jpg" class="header-mag" />
<img src="../images/Wash-Logo-Blue.png" class="main-logo" />
<span class="bar">
<h2><em style="color:#B11D65;">Subscribe Now</em> and save at least 50% off the newsstand price!</h2>
</span>
</div> <!-- #sub-header -->
</header>

<div id="subframe">
<form id="form" method="post" action="<?php print $form_action_location;?>">

<div class="form-error hidden">
<div id="subscribe_offer"></div>
<div id="firstname"></div>
<div id="lastname"></div>
<div id="address1"></div>
<div id="address2"></div>
<div id="city"></div>
<div id="state"></div>
<div id="zip"></div>
<div id="email"></div>
<div id="payment_method"></div>
<div id="payment_cc"></div>
<div id="payment_exp_month"></div>
<div id="payment_exp_year"></div>
</div>

<div id="sub-options" class="clearfix left">
<h3 style="color:#B11D65;"><img src="../images/number1.png" class="number" /> Choose Your Subscription</h3>
<div class="left default" style="position:relative;">
<img src="../images/best-offer.gif" class="offer-tag" />
<h4>Print + Digital Access</h4>
<p>Get the best of both worlds with the convenience of home delivery each month and instant access to the WASHINGTONIAN digital edition with exclusive bonus content. Select your term below.*</p>
<div class="product-block">
<img src="../images/cover/2014_Feb_small.jpg" class="magazine" /> <img src="../images/cover/iPad-cover-feb.png" class="tablet" />
</div>
<span style="margin-left:125px;">
<span><input type="radio" value="1" name="subscribe_offer" class="all" data-describedby="subscribe_offer" data-description="subscribe_offer" data-required="true"/> <label>1 year for $29.95 (12 issues)</label></span>
<span><input type="radio" value="2" name="subscribe_offer" class="all" /> <label>2 years for $49.95 (24 issues)</label></span>
</span>
<!-- <span class="right" style="width:59%; margin-left:0;">
<span><input type="radio" value="3" name="subscribe_offer" class="all" /> <label>Outside DC, MD, & VA: 1 year for $39.95</label></span>
<span><input type="radio" value="4" name="subscribe_offer" class="all" /> <label>Outside DC, MD, & VA: 2 years for $69.95</label></span> 
</span> -->
</div>

<div class="right default">
<h4>Digital Access</h4>
<p>Get instant access to WASHINGTONIAN content. Read the magazine on your iPad or computer&mdash;anytime!</p>
<img src="../images/cover/iPad-cover-feb.png" />
<span style="margin-left:85px;"><input type="radio" value="5" name="subscribe_offer" class="digital" /> <label>1 year for $19.99/year*</label></span>
</div>
</div> <!-- #sub-options -->

<p class="instant-access">Instant access to the WASHINGTONIAN digital edition is available on the iPad<sup>&reg;</sup> and via our website at <a href="http://www.washingtonian.com/tablet/" target="_blank">washingtonian.com/tablet</a></p>

<div id="sub-extras">

</div> <!-- #sub-extras -->

<div id="sub-data" class="clearfix">
<h3><img src="../images/number2.png" class="number" /> Mailing Address</h3>
<div class="default">
<span><label>First Name</label> <input type="text" name="firstname[]" data-describedby="firstname" data-validate="alpha" data-description="firstname" data-required="true" />
<label style="width:90px;">Last Name</label> <input type="text" name="lastname[]" data-describedby="lastname" data-validate="alpha" data-description="lastname" data-required="true" />
</span>
<span><label>Address</label> <input type="text" name="address1[]" data-describedby="address1" data-validate="alphanumeric" data-description="address1" data-required="true" /></span>
<span><label>Address 2</label> <input type="text" name="address2[]" data-describedby="address2" data-validate="alphanumeric" data-description="address2" /></span>
<span><label>City</label> <input type="text" name="city[]" data-describedby="city" data-validate="alpha" data-description="city" data-required="true" /></span>
<span><label>State</label> 

<select name="state[]" data-describedby="state" data-description="state" data-required="true">
            <option value="" selected="SELECTED">Select A State</option>
            <option value="AA" >APO/FPO-Americas</option><option value="AE" >APO/FPO-Middle East</option><option value="AP" >APO/FPO-Pacific</option><option value="AK" >Alaska</option><option value="AL" >Alabama</option><option value="AR" >Arkansas</option><option value="AS" >American Samoa</option><option value="AZ" >Arizona</option><option value="CA" >California</option><option value="CO" >Colorado</option><option value="CT" >Connecticut</option><option value="DC" >District of Columbia</option><option value="DE" >Delaware</option><option value="FL" >Florida</option><option value="GA" >Georgia</option><option value="GU" >Guam</option><option value="HI" >Hawaii</option><option value="IA" >Iowa</option><option value="ID" >Idaho</option><option value="IL" >Illinois</option><option value="IN" >Indiana</option><option value="KS" >Kansas</option><option value="KY" >Kentucky</option><option value="LA" >Louisiana</option><option value="MA" >Massachusetts</option><option value="MD" >Maryland</option><option value="ME" >Maine</option><option value="MI" >Michigan</option><option value="MN" >Minnesota</option><option value="MO" >Missouri</option><option value="MS" >Mississippi</option><option value="MT" >Montana</option><option value="NC" >North Carolina</option><option value="ND" >North Dakota</option><option value="NE" >Nebraska</option><option value="NH" >New Hampshire</option><option value="NJ" >New Jersey</option><option value="NM" >New Mexico</option><option value="NV" >Nevada</option><option value="NY" >New York</option><option value="OH" >Ohio</option><option value="OK" >Oklahoma</option><option value="OR" >Oregon</option><option value="PA" >Pennsylvania</option><option value="PR" >Puerto Rico</option><option value="RI" >Rhode Island</option><option value="SC" >South Carolina</option><option value="SD" >South Dakota</option><option value="TN" >Tennessee</option><option value="TX" >Texas</option><option value="UT" >Utah</option><option value="VA" >Virginia</option><option value="VT" >Vermont</option><option value="WA" >Washington</option><option value="WI" >Wisconsin</option><option value="WV" >West Virginia</option><option value="WY" >Wyoming</option>
          </select> 
          
          
        <label style="width:90px;">ZIP</label> <input type="text" name="zip[]" style="width:50px;" data-describedby="zip" data-validate="zip" data-description="zip" data-required="true" /></span>
        
<span><label>Email</label> <input type="text" name="email[]" data-describedby="email" data-validate="email" data-description="email" data-required="true" />
<p class="small-type">Will be used to send information in accordance with our Privacy Policy</p></span>

<span><input type="hidden" name="mailing_diff_recipient" value="0" /><input type="checkbox" name="mailing_diff_recipient" id="show-mailing" value="1" /> <em>Click here if billing address differs from mailing address</em></span>

<div id="mailing-address" class="left">
<h3>Billing Address</h3>
<span><label>Name</label> <input type="text" name="firstname[]" /></span>
<span><label>Address</label> <input type="text" name="address1[]" /></span>
<span><label>Address 2</label> <input type="text" name="address2[]" /></span>
<span><label>City</label> <input type="text" name="city[]" /></span>
<span><label>State</label> 

<select name="state[]">
            <option value="" selected="SELECTED">Select A State</option>
            <option value="AA" >APO/FPO-Americas</option><option value="AE" >APO/FPO-Middle East</option><option value="AP" >APO/FPO-Pacific</option><option value="AK" >Alaska</option><option value="AL" >Alabama</option><option value="AR" >Arkansas</option><option value="AS" >American Samoa</option><option value="AZ" >Arizona</option><option value="CA" >California</option><option value="CO" >Colorado</option><option value="CT" >Connecticut</option><option value="DC" >District of Columbia</option><option value="DE" >Delaware</option><option value="FL" >Florida</option><option value="GA" >Georgia</option><option value="GU" >Guam</option><option value="HI" >Hawaii</option><option value="IA" >Iowa</option><option value="ID" >Idaho</option><option value="IL" >Illinois</option><option value="IN" >Indiana</option><option value="KS" >Kansas</option><option value="KY" >Kentucky</option><option value="LA" >Louisiana</option><option value="MA" >Massachusetts</option><option value="MD" >Maryland</option><option value="ME" >Maine</option><option value="MI" >Michigan</option><option value="MN" >Minnesota</option><option value="MO" >Missouri</option><option value="MS" >Mississippi</option><option value="MT" >Montana</option><option value="NC" >North Carolina</option><option value="ND" >North Dakota</option><option value="NE" >Nebraska</option><option value="NH" >New Hampshire</option><option value="NJ" >New Jersey</option><option value="NM" >New Mexico</option><option value="NV" >Nevada</option><option value="NY" >New York</option><option value="OH" >Ohio</option><option value="OK" >Oklahoma</option><option value="OR" >Oregon</option><option value="PA" >Pennsylvania</option><option value="PR" >Puerto Rico</option><option value="RI" >Rhode Island</option><option value="SC" >South Carolina</option><option value="SD" >South Dakota</option><option value="TN" >Tennessee</option><option value="TX" >Texas</option><option value="UT" >Utah</option><option value="VA" >Virginia</option><option value="VT" >Vermont</option><option value="WA" >Washington</option><option value="WI" >Wisconsin</option><option value="WV" >West Virginia</option><option value="WY" >Wyoming</option>
          </select></span> 
        <span><label>ZIP</label> <input type="text" style="width:50px;" name="zip[]" /></span>
</div>

</div>


<div class="payment">
<h4>Please Let Us Know How You Would Prefer to Pay</h4>
<span class="clearfix"><label>Payment Method</label> <select name="payment_method" id="cc_cards" data-describedby="payment_method" data-description="payment_method" data-required="true">
                <option value="" selected="selected">Select a payment method</option>
                <option value="mastercard">MasterCard</option>
                <option value="visa">Visa</option>
				<option value="amex">American Express</option>
              </select>
</span>
<span class="clearfix"><label>Credit Card Number</label> <input type="text" name="payment_cc" data-describedby="payment_cc" data-validate="cc" data-description="payment_cc" data-required="true" /></span>
<span class="clearfix"><label>Expiration Date</label> 
<select name="payment_exp_month" style="width:90px;" data-describedby="payment_exp_month" data-description="payment_exp_month" data-required="true">
              <option value="" selected="selected">Month</option>
              <option value="01">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
		</select>
        
        <select name="payment_exp_year" SIZE="1" class="input_98" style="width:80px; margin-left:10px;" data-describedby="payment_exp_year" data-description="payment_exp_year" data-required="true">
              <option value="" selected="selected">Year</option>
              <option value="13" >2013</option><option value="14" >2014</option><option value="15" >2015</option><option value="16" >2016</option><option value="17" >2017</option><option value="18" >2018</option><option value="19" >2019</option><option value="20" >2020</option><option value="21" >2021</option><option value="22" >2022</option><option value="23" >2023</option>
            </select>
</span>
<img src="../images/credit-cards.png" /> <img src="../images/secure-trans.png" width="126" class="secure" />
</div>
</div>

<p class="renewal">
<strong>* Automatic Renewal Program:</strong> Your subscription will be automatically renewed unless you tell us to stop. Before the start of each renewal, you will be sent a reminder notice stating the term and rate then in effect. If you do nothing, your credit/debit card will be charged or you will be sent an invoice for your subscription. You may cancel at any time during subscription and receive a full refund for unmailed issues. 
</p>

<INPUT TYPE="HIDDEN" name="promotionkey" value="<?php print $promotionKey['special']['a']; ?>"/> 

<input type="submit" id="submit" value="subscribe" />
</form> <!-- #form -->

<div id="sub-contacts">
<p>Log in to your <a href="https://w1.buysub.com/pubs/WH/WSH/Combo3Entry.jsp?cds_page_id=22265">Subscriber Services</a> account to update your subscription settings.<br /> Or call 202-331-0715 to speak with a member of our circulation team.</p>

<p class="smaller">Use of this site constitutes acceptance of our User Agreement and Privacy Policy.</p>

<p class="smaller">To learn more about our information practices and your privacy rights, click here.</p>

<p class="smaller">Copyright &copy;2013 Washington Magazine, Inc. All rights reserved.</p>
</div> <!-- #sub-contacts -->

</div> <!-- #subframe -->

</div> <!-- #inner-wrapper -->
</div> <!-- #wrapper -->

<script type="text/javascript" src="../js/validate.js"></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1860879-1']);
  _gaq.push(['_setDomainName', 'washingtonian.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

//Bad Robot Detection

	var category = 'trafficQuality';
	var dimension = 'botDetection';
	var human_events = ['onkeydown','onmousemove'];

	if ( navigator.appName == 'Microsoft Internet Explorer' && !document.referrer) {
		for(var i = 0; i < human_events.length; i++){
			document.attachEvent(human_events[i], ourEventPushOnce);
		}
	}else{
		_gaq.push( [ '_trackEvent', category, dimension, 'botExcluded', 1, true ] );
	}

	function ourEventPushOnce(ev) {

		_gaq.push( [ '_trackEvent', category, dimension, 'on' + ev.type, 1, true ] );

		for(var i = 0; i < human_events.length; i++){
			document.detachEvent(human_events[i], ourEventPushOnce);
		}

	} // end ourEventPushOnce()

	//End Bad Robot Detection

</script>


</body>
</html>