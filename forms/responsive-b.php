<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Subscribe | Washingtonian</title>
		<meta charset="UTF-8">
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="../css/responsive-b.css">
	</head>
	<body>
		<div class="container">
			<div class="col-xs-12">
				<div class="wrapper">
					<div class="body">
						<img src="http://localhost:8888/MAMP/projects/subs/images/cover/2014_Feb_small.jpg" class="img-responsive head hidden-xs hidden-sm" alt="">
						<header>
							<img src="../images/Wash-Logo-Blue.png" class="img-responsive" alt="Washingtonian">
						</header>
						<div class="bar">
							<h2><span>SUBSCRIBE NOW</span> AND SAVE AT LEAST 50% OFF THE NEWSSTAND PRICE!</h2>
						</div>
						<div class="ipad small">
							Instant access to the WASHINGTONIAN digital edition is available on the iPad® and via our website at <a href="http://www.washingtonian.com/tablet/">washingtonian.com/tablet</a>
						</div>
						<form action="" method="POST" class="form-horizontal" role="form">
							<div id="sub-options" class="subscription clearfix">
								<h3 class="options"><img src="http://www.washingtonian.com/magazine/subscribe/images/number1.png" alt="1"> Choose Your Subscription</h3>
								<div class="col-sm-6  col-md-7 col-lg-8">
									<div class="sub print text-center">
										<div class="value"><img src="../images/best-offer.gif" alt=""></div>
										<span>PRINT + DIGITAL ACCESS</span>
										<br>
										Get the best of both worlds with the convenience of home delivery each month and instant access to the WASHINGTONIAN digital edition with exclusive bonus content. Select your term below.*
										<br>
										<img src="../images/cover/2014_Feb_small.jpg" class="img-responsive magazine" alt="Image">
										<img src="../images/cover/iPad-cover-feb.png" class="img-responsive tablet" alt="">
										<div class="radio">
											<label>
												<input type="radio" name="subscribe_offer" id="input" value="1" class="all">
												1 year for $29.95 (12 issues)
											</label>
										</div>
										<div class="radio">
											<label>
												<input type="radio" name="subscribe_offer" id="input" value="2" class="all">
												2 years for $49.95 (24 issues)
											</label>
										</div>
									</div>
								</div>
								<div class="col-sm-6 col-md-5 col-lg-4">
									<div class="sub digital text-center">
										<span>DIGITAL ACCESS</span>
										<br>
										Get instant access to WASHINGTONIAN content. Read the magazine on your iPad or computer—anytime!
										<br>
										<img src="../images/cover/iPad-cover-feb.png" class="img-responsive tablet" alt="">
										<div class="radio">
											<label>
												<input type="radio" name="subscribe_offer" id="input" value="2" class="digital">
												 1 year for $19.99/year*
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="address clearfix">
								<h3 class="mail"><img src="http://www.washingtonian.com/magazine/subscribe/images/number2.png" alt="2"> Mailing Address</h3>
								<div class="form-group">
									<label for="inputFirstname" class="col-md-2 control-label">First Name:</label>
									<div class="col-md-4">
										<input type="text" name="firstname[]" id="inputFirstname" class="form-control" value="" required="required" pattern="" title="">
									</div>
									<label for="inputLastname" class="col-md-2 control-label">Last Name:</label>
									<div class="col-md-4">
										<input type="text" name="lastname[]" id="inputLastname" class="form-control" value="" required="required" pattern="" title="">
									</div>
								</div>

								<div class="form-group">
									<label for="inputAddress1" class="col-md-2 control-label">Address:</label>
									<div class="col-md-4">
										<input type="text" name="address1[]" id="inputAddress1" class="form-control" value="" required="required" pattern="" title="">
									</div>
								</div>
								
								<div class="form-group">
									<label for="inputAddress2" class="col-md-2 control-label">Address 2:</label>
									<div class="col-md-4">
										<input type="text" name="address2[]" id="inputAddress2" class="form-control" value="" required="required" pattern="" title="">
									</div>
								</div>

								<div class="form-group">
									<label for="inputCity" class="col-md-2 control-label">City:</label>
									<div class="col-md-4">
										<input type="text" name="city[]" id="inputCity" class="form-control" value="" required="required" pattern="" title="">
									</div>
								</div>

								<div class="form-group">
									<label for="inputState" class="col-md-2 control-label">State:</label>
									<div class="col-md-4">
										<select name="state" id="inputState" class="form-control">
											<option value="">-- Select One --</option>
										</select>
									</div>
									<label for="inputZip" class="col-md-2 control-label">Zip:</label>
									<div class="col-md-2">
										<input type="text" name="zip" id="inputZip" class="form-control" value="" required="required" pattern="" title="">
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail" class="col-md-2 control-label">Email:</label>
									<div class="col-md-4">
										<input type="text" name="email" id="inputEmail" class="form-control" value="" required="required" pattern="" title="">
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-10 col-sm-offset-2">
										Will be used to send information in accordance with our Privacy Policy
									</div>
								</div>
							
							</div>
							<div class="payment">
								<div class="form-group">
									<div class="col-sm-10 col-sm-offset-2">
										<h4>PLEASE LET US KNOW HOW YOU WOULD PREFER TO PAY</h4>
									</div>
								</div>

								<div class="form-group">
									<label for="inputMethod" class="col-md-2 control-label">Payment Method:</label>
									<div class="col-md-4">
										<select name="method" id="inputMethod" class="form-control">
											<option value="">-- Select One --</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label for="inputCardNumber" class="col-md-2 control-label">Credit Card Number</label>
									<div class="col-md-4">
										<input type="text" name="cardNumber" id="inputCardNumber" class="form-control" value="" required="required" pattern="" title="">
									</div>
								</div>

								<div class="form-group">
									<label for="inputExp" class="col-md-2 control-label">Expiration Date</label>
									<div class="col-md-2">
										<select name="exp" id="inputExp" class="form-control">
											<option value="">-- Select One --</option>
										</select>
									</div>
									<div class="col-md-2">
										<select name="exp" id="inputExp" class="form-control">
											<option value="">-- Select One --</option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-10 col-sm-offset-2">
										<img src="../images/credit-cards.png" alt=""> <img src="../images/secure-trans.png" alt="">
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-10 col-sm-offset-1 text-center small">
										* Automatic Renewal Program: Your subscription will be automatically renewed unless you tell us to stop. Before the start of each renewal, you will be sent a reminder notice stating the term and rate then in effect. If you do nothing, your credit/debit card will be charged or you will be sent an invoice for your subscription. You may cancel at any time during subscription and receive a full refund for unmailed issues.
									</div>
								</div>

								<div class="form-group">
									<div class="col-xs-12 text-center">
										<button type="submit" class="btn btn-danger btn-lg submit">Submit</button>
									</div>
								</div>
							</div>
						</form>
						<footer class="clearfix">
							<div class="col-sm-8 col-sm-offset-2 text-center">
								Log in to your Subscriber Services account to update your subscription settings.
								<br>
								Or call 202-331-0715 to speak with a member of our circulation team.
								<br>
								Use of this site constitutes acceptance of our User Agreement and Privacy Policy.
								<br>
								To learn more about our information practices and your privacy rights, click here.
								<br>
								Copyright ©2013 Washington Magazine, Inc. All rights reserved.
							</div>
						</footer>
					</div>
				</div>
			</div>
		</div>

		<!-- jQuery -->
		<script src="http://code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
	</body>
</html>