<?php require_once('../inc/config.inc'); ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Subscribe | Washingtonian</title>
		<meta charset="UTF-8">
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="../css/responsive.css">
	</head>
	<body>
		<div class="container">
			<div class="body">
				<div class="sub hidden-xs">MAGAZINE SUBSCRIPTION FORM</div>
				<div class="clearfix">
					<div class="col-sm-12 col-md-9">
						<form action="<?php print $form_action_location;?>" method="POST" role="form">
							<div class="row head text-center"><a href="http://www.washingtonian.com"><img src="../images/Wash-Logo-Blue.png" alt="Washingtonian Magazine" height="66"></a><span class="save"><span class="size-16">Save</span><span class="size-13">up to</span><span class="size-19">50%</span></div>
							<div class="bold p-bottom"><span class="red">Fill in the form below</span> to subscribe to Washingtonian for up to 50% OFF what others pay on the newsstand — that's like getting 6 FREE issues!</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="sr-only" for="firstname">First Name</label>
									<input type="text" name="firstname[]" class="form-control" id="firstname" placeholder="First Name" required="required">
								</div>
								<div class="form-group">
									<label class="sr-only" for="lastname">Last Name</label>
									<input type="text" name="lastname[]" class="form-control" id="lastname" placeholder="Last Name" required="required">
								</div>
								<div class="form-group">
									<label class="sr-only" for="address1">Address Line 1</label>
									<input type="text" name="address1[]" class="form-control" id="address1" placeholder="Address Line 1" required="required">
								</div>
								<div class="form-group">
									<label class="sr-only" for="address2">Address Line 2</label>
<<<<<<< HEAD
									<input type="text" name="address2[]" class="form-control" id="address2" placeholder="Address Line 2">
=======
									<input type="text" name="address2[]" class="form-control" id="address2" placeholder="Address Line 2" required="required">
>>>>>>> 6d66207af8846206666834e78d7eccc92400aedc
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label class="sr-only" for="city">City</label>
									<input type="text" name="city[]" class="form-control" id="city" placeholder="City" required="required">
								</div>
								<div class="form-group">
									<label class="sr-only" for="lastname">State</label>
									<select name="state[]" id="state" class="form-control" required="required">
										<option value="">State</option>
										<option value="AA">APO/FPO-Americas</option>
										<option value="AE">APO/FPO-Middle East</option>
										<option value="AP">APO/FPO-Pacific</option>
										<option value="AK">Alaska</option>
										<option value="AL">Alabama</option>
										<option value="AR">Arkansas</option>
										<option value="AS">American Samoa</option>
										<option value="AZ">Arizona</option>
										<option value="CA">California</option>
										<option value="CO">Colorado</option>
										<option value="CT">Connecticut</option>
										<option value="DC">District of Columbia</option>
										<option value="DE">Delaware</option>
										<option value="FL">Florida</option>
										<option value="GA">Georgia</option>
										<option value="GU">Guam</option>
										<option value="HI">Hawaii</option>
										<option value="IA">Iowa</option>
										<option value="ID">Idaho</option>
										<option value="IL">Illinois</option>
										<option value="IN">Indiana</option>
										<option value="KS">Kansas</option>
										<option value="KY">Kentucky</option>
										<option value="LA">Louisiana</option>
										<option value="MA">Massachusetts</option>
										<option value="MD">Maryland</option>
										<option value="ME">Maine</option>
										<option value="MI">Michigan</option>
										<option value="MN">Minnesota</option>
										<option value="MO">Missouri</option>
										<option value="MS">Mississippi</option>
										<option value="MT">Montana</option>
										<option value="NC">North Carolina</option>
										<option value="ND">North Dakota</option>
										<option value="NE">Nebraska</option>
										<option value="NH">New Hampshire</option>
										<option value="NJ">New Jersey</option>
										<option value="NM">New Mexico</option>
										<option value="NV">Nevada</option>
										<option value="NY">New York</option>
										<option value="OH">Ohio</option>
										<option value="OK">Oklahoma</option>
										<option value="OR">Oregon</option>
										<option value="PA">Pennsylvania</option>
										<option value="PR">Puerto Rico</option>
										<option value="RI">Rhode Island</option>
										<option value="SC">South Carolina</option>
										<option value="SD">South Dakota</option>
										<option value="TN">Tennessee</option>
										<option value="TX">Texas</option>
										<option value="UT">Utah</option>
										<option value="VA">Virginia</option>
										<option value="VT">Vermont</option>
										<option value="WA">Washington</option>
										<option value="WI">Wisconsin</option>
										<option value="WV">West Virginia</option>
										<option value="WY">Wyoming</option>
									</select>
								</div>
								<div class="form-group">
									<label class="sr-only" for="zip">Zip/Postal Code</label>
									<input type="text" name="zip[]" class="form-control" id="zip" placeholder="Zip/Postal Code">
								</div>
								<div class="form-group">
									<label class="sr-only" for="email">Email Address</label>
									<input type="email" name="email[]" class="form-control" id="email" placeholder="Email Address">
								</div>
							</div>

							<div class="col-sm-6">
								<h4 class="red bold">Select Term:<br><small>Each term includes the features of the <strong>Automatic Renewal Program*</strong></small></h4>
								<div class="radio">
									<label>
										<input type="radio" name="subscribe_offer" id="subscribe_offer" value="1" checked="checked">
										<strong>Print + Digital &mdash;</strong> 1 year for $29.95 (12 issues)
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="subscribe_offer" id="subscribe_offer" value="2">
										<strong>Print + Digital &mdash;</strong> 2 years for $49.95 (24 issues)
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="subscribe_offer" id="subscribe_offer" value="5">
										<strong>Digital Only &mdash;</strong> 1 year for $19.99/year*
									</label>
								</div>
								<div class="payment">
									<h4 class="red bold">Select Secure Payment Method</h4>
									<div class="grey">
										<div class="form-group">
											<label class="sr-only" for="lastname">Payment Method</label>
											<select name="payment_method" id="payment_method" class="form-control" required="required">
												<option value="" selected="selected">Select payment method</option>
												<option value="mastercard">MasterCard</option>
												<option value="visa">Visa</option>
												<option value="amex">American Express</option>
											</select>
										</div>
										<div class="form-group">
											<label class="sr-only" for="payment_cc">Credit Card Number</label>
											<input type="text" name="payment_cc" class="form-control" id="payment_cc" placeholder="Credit Card Number">
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-xs-6">
													<label class="sr-only" for="payment_exp_month">Expiration Month</label>
													<select type="text" name="payment_exp_month" class="form-control" id="payment_exp_month">
														<option value="" selected="selected">Month</option>
														<option value="01">01</option>
														<option value="02">02</option>
														<option value="03">03</option>
														<option value="04">04</option>
														<option value="05">05</option>
														<option value="06">06</option>
														<option value="07">07</option>
														<option value="08">08</option>
														<option value="09">09</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
													</select>
												</div>
												<div class="col-xs-6">
													<label class="sr-only" for="payment_exp_year">Expiration Year</label>
													<select type="text" name="payment_exp_year" class="form-control" id="payment_exp_year">
														<option value="" selected="selected">Year</option>
														<option value="14" >2014</option>
														<option value="15" >2015</option>
														<option value="16" >2016</option>
														<option value="17" >2017</option>
														<option value="18" >2018</option>
														<option value="19" >2019</option>
														<option value="20" >2020</option>
														<option value="21" >2021</option>
														<option value="22" >2022</option>
														<option value="23" >2023</option>
													</select>
												</div>
											</div>
										</div>
										<img src="../images/credit-cards.png" />
										<img src="../images/secure-trans.png" width="126" class="secure" />
									</div>
								</div>
								<div class="form-group">
									<label class="sr-only" for="promo_code">Promo Code</label>
									<input type="text" name="promo_code" class="form-control" id="promo_code" placeholder="Promo Code">
								</div>
							</div>
							<div class="col-sm-6 hidden-xs text-center m-top">
<<<<<<< HEAD
								<img class="img-thumbnail" height="375" alt="Washingtonian Magazine" src="../images/cover/latest.jpg">
=======
								<img class="img-thumbnail" alt="Washingtonian Magazine" src="../images/cover/2014_Feb_small.jpg">
>>>>>>> 6d66207af8846206666834e78d7eccc92400aedc
							</div>
							<div class="col-xs-12 small">
								<strong>*Automatic Renewal Program</strong>: Your subscription will be automatically renewed unless you tell us to stop. Before the start of each renewal, you will be sent a reminder notice stating the term and rate then in effect. If you do nothing, your credit/debit card will be charged or you will be sent an invoice for your subscription. You may cancel at any time during subscription and receive a full refund for unmailed issues.
							</div>
							
							<div class="col-xs-12 text-center">
<<<<<<< HEAD
								<input type="hidden" name="promotionkey" value="<?php print $promotionKey['responsive']['a']; ?>">
=======
								<input type="hidden" name="promotionkey" value="<?php print $promotionKey['special']['b']; ?>">
>>>>>>> 6d66207af8846206666834e78d7eccc92400aedc
								<button type="submit" class="btn btn-danger m-top">Submit</button>
							</div>
						</form>
	
					</div>
					<div class="col-md-3 side">&nbsp;</div>
				</div>
			</div>
			<div class="footer">
				<img class="hidden-xs" src="../images/Wash-Logo-Blue.png" height="20" />
 				<p class="small" style="margin-top:5px;">&copy;2013 Washington Magazine Company. 1828 L St. NW #200, Washington, DC 20036</p>
 				<p class="small" style="margin-top:8px;"><a style="color:#085899;" href="http://www.washingtonian.com/privacy-policy/">Privacy Policy</a> &nbsp; | &nbsp; <a style="color:#085899;" href="http://www.washingtonian.com/contact-us/">Contact Us</a></p>
			</div>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<<<<<<< HEAD
		<script type="text/javascript">

			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-1860879-1']);
			_gaq.push(['_setDomainName', 'washingtonian.com']);
			_gaq.push(['_trackPageview']);

			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

			//Bad Robot Detection

			var category = 'trafficQuality';
			var dimension = 'botDetection';
			var human_events = ['onkeydown','onmousemove'];

			if ( navigator.appName == 'Microsoft Internet Explorer' && !document.referrer) {
				for(var i = 0; i < human_events.length; i++){
					document.attachEvent(human_events[i], ourEventPushOnce);
				}
			}else{
				_gaq.push( [ '_trackEvent', category, dimension, 'botExcluded', 1, true ] );
			}

			function ourEventPushOnce(ev) {

				_gaq.push( [ '_trackEvent', category, dimension, 'on' + ev.type, 1, true ] );

				for(var i = 0; i < human_events.length; i++){
					document.detachEvent(human_events[i], ourEventPushOnce);
				}

			} // end ourEventPushOnce()

			//End Bad Robot Detection

		</script>
=======
>>>>>>> 6d66207af8846206666834e78d7eccc92400aedc
	</body>
</html>