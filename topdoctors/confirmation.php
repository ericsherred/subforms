<?php 

    $email                =   $_GET['email'];
    $firstname              =   $_GET['firstname'];
    $lastname             =   $_GET['lastname'];
    $address1             =   $_GET['address1'];
    $address2             =   $_GET['address2'];
    $city               =   $_GET['city'];
    $state                =   $_GET['state'];
    $zipcode              =   $_GET['zipcode'];
    $country              =   $_GET['country'];
    $value                =   $_GET['value'];
    $date               =   date_create();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" type="text/css" href="css/special-form-b.css" />
<link rel="stylesheet" href="fonts/fontello_V7/css/fontello.css">
    <link rel="stylesheet" href="fonts/fontello_V7/css/animation.css"><!--[if IE 7]>
    <link rel="stylesheet" href="fonts/fontello_V7/css/fontello-ie7.css"><![endif]-->

<link type="text/css" rel="stylesheet" href="css/washingtonian_CDS.css" media="screen" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
<script src="../js/jquery-validate.min.js"></script>

<title>Subscribe | Washingtonian</title>
</head>

<body>
<div id="sub-wrapper" class="clearfix">
<div id="top-sub-bar">
<h4>Magazine Subscription Form <i class="icon-down-dir"></i></h4>
</div> <!-- #top-sub-bar -->

<div id="cntr">
<a href="http://washingtonian.com"><img src="images/Wash-Logo-Blue.png" height="66" id="wash-logo" /></a>

        <div id="section-article" style="clear:both;">   
        
  
<div class="CDS_main_area">
      <div id="mag_header_area">
        <img src="http://www.washingtonian.com/articles/images/2014_Feb_small.jpg" style="margin-right:5px;" alt="latest cover" width="95" align="left" />
           <div id="callout"><h1>Thank you from Washingtonian Magazine</h1>
            <em>Your source for dining, nightlife, news, shopping and more in the Washington region.</em>
            </div>
            <div class="clear"></div>

        </div>
        <div id="gift_recipients"><!--Begin Gift Recipients-->
          <table border="0" cellpadding="0" cellspacing="0" class="whiteborder">
        <tr>
          
          
        </tr>
      </table>
    
    </div>
        
        <form name="convertible_form" action="../order-processing-curl.php" method="post">
<!-- RESPONSE KEY:  I12SR -->
<INPUT TYPE="HIDDEN" NAME="lsid" VALUE="21221317436015952">
<INPUT TYPE="HIDDEN" NAME="vid" VALUE="1">
<INPUT TYPE="HIDDEN" NAME="cds_page_id" VALUE="60116">
<INPUT TYPE="HIDDEN" NAME="cds_mag_code" VALUE="WSH">
<INPUT TYPE="HIDDEN" NAME="cds_form_submitted" VALUE="WSH_Convertible:convertible_form">
<INPUT TYPE="HIDDEN" NAME="cds_doms_order_number" VALUE="">
        <div style="color:#F00;">
          
        </div>
        <div class="box_bkg">
       
       <!-- BEGIN SUBSCRIPTION TYPE INFORMATION -->
        <div id="Payment_info"><!--Begin Subscription Info-->
        
        

        <div class="CF_headline" style="margin-top:10px;">Subscription Confirmation</div>
         
        <div class="confirmation-text">
          
          <p>Thank you for becoming a digital subscriber to Washingtonian.</p>
          <p>To log into your iPad edition, please <a href="#">click here</a>.</p>
          <p> User Name: <?php print $email;?> <br/>
            Password: <?php print $zipcode;?> 
          </p>
          <p>If you have any questions, do not hesitate to contact us.</p>
          <p> 202-331-0715 M-F 9a-5p EST <br/>
            800-365-7050 M-F 8a-10:30p, Sat 9a-7p EST <br/>
            <a href="mailto:washsub@washingtonian.com">washsub@washingtonian.com</a>
          </p>
          <p>
          
          <div class="CF_headline" style="margin-top:10px;">Billing Information</div>
          
          <p>
            <strong>Name:</strong> <?php print $firstname;?> <?php print $lastname;?><br/>
            <strong>Address:</strong> <?php print $address1 . "<br/>" . $address2;?> <br/>
            <strong>City:</strong> <?php print $city;?> <br/>
            <strong>State:</strong> <?php print $state;?> <br/>
            <strong>Zip/Postal Code:</strong> <?php print $zipcode;?> <br/>
            <strong>Email Address:</strong> <?php print $email;?> <br/>
          </p>
          
        </div>
        
        
      </div><!-- end Subscription Type -->
      
      
        
        
        <div id="Payment_info"><!--Begin Payment Info-->
          <div id="TV">


   </div>
        
            
          <!--End Gift Checkbox-->
          </div><!--End Payment Info-->
          
          <div style="float:right;"><a href="http://www.washingtonian.com">Return to Washingtonian.com >></a></div>
          <div class="clear"></div>
        </div>
        
        <!-- DONEE_INFO DELETED WITHIN THIS SECTION -->

    
    </form>
    
  </div>
</div>

</div> <!-- #cntr -->

<div id="side">
</div> <!-- #side -->

</div> <!-- #sub-wrapper -->

 <div id="footer">
 <img src="images/Wash-Logo-Blue.png" height="20" />
 <p class="fine-print" style="margin-top:5px;">&copy;2013 Washington Magazine Company. 1828 L St. NW #200, Washington, DC 20036</p>
 <p class="fine-print" style="margin-top:8px;"><a style="color:#085899;" href="http://www.washingtonian.com/privacy-policy/">Privacy Policy</a> &nbsp; | &nbsp; <a style="color:#085899;" href="http://www.washingtonian.com/contact-us/">Contact Us</a></p>
 </div> <!-- footer -->

<script type="text/javascript" src="../js/validate.js"></script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1860879-1']);
  _gaq.push(['_setDomainName', 'washingtonian.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

//Bad Robot Detection

	var category = 'trafficQuality';
	var dimension = 'botDetection';
	var human_events = ['onkeydown','onmousemove'];

	if ( navigator.appName == 'Microsoft Internet Explorer' && !document.referrer) {
		for(var i = 0; i < human_events.length; i++){
			document.attachEvent(human_events[i], ourEventPushOnce);
		}
	}else{
		_gaq.push( [ '_trackEvent', category, dimension, 'botExcluded', 1, true ] );
	}

	function ourEventPushOnce(ev) {

		_gaq.push( [ '_trackEvent', category, dimension, 'on' + ev.type, 1, true ] );

		for(var i = 0; i < human_events.length; i++){
			document.detachEvent(human_events[i], ourEventPushOnce);
		}

	}  // end ourEventPushOnce()

	//End Bad Robot Detection

</script>

</body>
</html>