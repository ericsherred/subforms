<?php

	/**
	* Offer: Defines and returns Washingtonin subcription offers.
	*/
	class Offer
	{
		protected $_price;
		protected $_term;
		protected $_description;
		protected $_isLocal;
		protected $_productCode;
		public $_aditional;
				
		public function setOffer($val)
		{
			
			switch($val) {
				case 1:
					$this->_price			=	29.95;
					$this->_term			=	12;
					$this->_description		=	"DC, MD, & VA: 1 Year (12 issues) for $29.95";
					$this->_isLocal			=	true;
					$this->_productCode		=	"C"; //Print + Digital
					$this->_aditional		=	1;
					break;
				case 2:
					$this->_price			=	49.95;
					$this->_term			=	24;	
					$this->_description		=	"DC, MD, & VA: 2 Years (24 issues) for $49.95";	
					$this->_isLocal			=	true;	
					$this->_productCode		=	"C"; //Print + Digital
					$this->_aditional		=	2;
					break;
				case 3:
					$this->_price			=	39.95;
					$this->_term			=	12;
					$this->_description		=	"Outside DC, MD, & VA: 1 Year (12 issues) for $39.95";
					$this->_isLocal			=	false;
					$this->_productCode		=	"C"; //Print + Digital
					$this->_aditional		=	3;
					break;
				case 4:
					$this->_price			=	69.95;
					$this->_term			=	24;
					$this->_description		=	"Outside DC, MD, & VA: 2 Years (24 issues) for $69.95";
					$this->_isLocal			=	false;
					$this->_productCode		=	"C"; //Print + Digital
					$this->_aditional		=	4;
					break;
				case 5:
					$this->_price			=	19.99;
					$this->_term			=	12;
					$this->_description		=	"iPad and Digital Only for $19.99";
					$this->_isLocal			=	true;
					$this->_productCode		=	"I"; //Digital Only
					$this->_aditional		=	5;
					break;
				case 6:
					$this->_price			=	14.00;
					$this->_term			=	12;
					$this->_description		=	"1 Year (12 issues) for $14.00";
					$this->_productCode		=	"C"; //Print + Digital
					$this->_isLocal			=	true;
					$this->_aditional		=	6;
					break;
				case 7: 
					$this->_price			=	9.99;
					$this->_term			=	12;
					$this->_description		=	"iPad and Digital Only for $9.99";
					$this->_isLocal			=	true;
					$this->_productCode		=	"I"; //Digital Only
					$this->_aditional		=	7;
					break;
				case 8: // Holiday Special - First is $29.95, every one after is $19.95 (Case 8 and 9)
					$this->_price			=	29.95;
					$this->_term			=	12;
					$this->_description		=	"1 Year (12 issues) for $29.95";
					$this->_productCode		=	"C"; //Print + Digital
					$this->_isLocal			=	true;
					$this->_aditional		=	9;
					break;
				case 9:
					$this->_price			=	19.95;
					$this->_term			=	12;
					$this->_description		=	"1 Year (12 issues) for $19.95";
					$this->_productCode		=	"C"; //Print + Digital
					$this->_isLocal			=	true;
					$this->_aditional		=	9;
					break;
				case 10: // Holiday Special - First is $19.99, every one after is $9.99 (Case 10 and 11)
					$this->_price			=	19.99;
					$this->_term			=	12;
					$this->_description		=	"iPad and Digital Only for $19.99";
					$this->_isLocal			=	true;
					$this->_productCode		=	"I"; //Digital Only
					$this->_aditional		=	11;
					break;
				case 11:
					$this->_price			=	9.99;
					$this->_term			=	12;
					$this->_description		=	"iPad and Digital Only for $9.99";
					$this->_isLocal			=	true;
					$this->_productCode		=	"I"; //Digital Only
					$this->_aditional		=	11;
					break;
				case 12:
					$this->_price			=	18.00;
					$this->_term			=	12;
					$this->_description		=	"1 Year (12 issues) for $18";
					$this->_isLocal			=	true;
					$this->_productCode		=	"C"; //Print + Digital
					$this->_aditional		=	12;
					break;
				default: 
					$this->_price			=	29.95;
					$this->_term			=	12;
					$this->_description		=	"DC, MD, & VA: 1 Year (12 issues) for $29.95";
					$this->_isLocal			=	true;
					$this->_productCode		=	"C"; //Print + Digital
					$this->_aditional		=	0;
			}
		}
	
		public function getOfferXML() {
			$xml 	=	"<subscriptionTerm>".$this->_term."</subscriptionTerm>";
			$xml 	.=	"<value>".$this->_price."</value>";

			return $xml;
		}

		public function getOfferCodeXML() {
			$xml 	=	"<specialProductCode>".$this->_productCode."</specialProductCode>";

			return $xml;
		}
		
		public function returnOffer() {
			$order = array(
				'price' => $this->_price, 
				'term' => $this->_term, 
				'description' => $this->_description,
				'isLocal' => $this->_isLocal,
				'productCode' => $this->_productCode
			);

			return $order;
		}

		public function getPrice() {
			return $this->_price;
		}

		public function getTerm() {
			return $this->_term;
		}

		public function getDescription() {
			return $this->_description;
		}

		public function getIsLocal() {
			return $this->isLocal;
		}

		public function getProductCode() {
			return $this->_productCode;
		}
	}