<?php 

// Sets the location of the form action script. Since this is referenced across multiple forms. 
//$form_action_location				=	'_form_processing.php';
// $form_action_location				=	'inc/process.form.php';
$form_action_location				=	'inc/process.form.php';

// Promotion Keys -- obtain these from Sara Fox in Circulation
// These keys should be updated monthly. 
// Last updated: September 2013

$promotionKey['control']['a']		=	'I13LSP1'; // Feb 2014
$promotionKey['control']['b']		=	'I13LSP2'; // Feb 2014

$promotionKey['special']['a']		=	'I13LSP3'; // Feb 2014
$promotionKey['special']['b']		=	'I13LSP4'; // Feb 2014

$promotionKey['custom']['doc']		=	'I14TDEB';

$promotionKey['holiday']['a']		=	'I13HCMEB';
$promotionKey['holiday']['b']		=	'X13MI';

$adminEmail							=	'mbender@washingtonian.com';

?>