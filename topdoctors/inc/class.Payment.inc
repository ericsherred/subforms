<?php

	/**
	* Payment: Creates and holds a payment pbject to contain credit card information
	*/
	class Payment
	{
		protected $_cardType;
		protected $_cardNum;
		protected $_expMonth;
		protected $_expYear;

		function __construct($data = array())
		{
			$this->_cardType 	= 	$data['type'];
			$this->_cardNum		=	$data['number'];
			$this->_expMonth	=	$data['expM'];
			$this->_expYear		=	$data['expY'];
		}

		public function getPaymentXML() {
			$xml	=		"<payment>";
			$xml	.=			"<paymentType>Credit Card</paymentType>";
			$xml	.=			"<creditCardType>".$this->_cardType."</creditCardType>";
			$xml	.=			"<creditCardNumber>".$this->_cardNum."</creditCardNumber>";
			$xml	.=			"<creditCardExpire>".$this->_expMonth.$this->_expYear."</creditCardExpire>";
			$xml	.=		"</payment>";

			return $xml;
		}

		public function getCardType() {
			return $this->_cardType;
		}

		public function getCardNum() {
			return $this->_cardNum;
		}

		public function getExpMonth() {
			return $this->_expMonth;
		}

		public function getExpYear() {
			return $this->_expYear;
		}
	}