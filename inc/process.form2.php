<?php

	include('class.Order2.inc');

	$data = $_POST;

	// Validate Data

	$order = new Order();

	$order->setOrder($data);

	$order->setXML();

	$order->submitOrder();

	if ($order->isSuccess() == 'true') {
		$order->submitToDB();

	} else {
		echo "<h1>There was an error processing your order. Please try again shortly.</h1>";
	}

?>
