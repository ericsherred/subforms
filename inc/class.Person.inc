<?php

	include('class.Offer.inc');
	include('class.Promo.inc');


	/**
	* Person: describes and sets an individual and their contact information
	*/
	class Person
	{
		protected $_firstname;
		protected $_lastname;
		protected $_address1;
		protected $_address2;
		protected $_city;
		protected $_state;
		protected $_zip;
		protected $_email;
		protected $_offer;
		
		public function setPerson($data = array()) {
			$this->setName($data['first'],$data['last']);
			$this->setAddress($data['address1'],$data['address2'],$data['city'],$data['state'],$data['zip']);
			$this->setEmail($data['email']);
			$this->_offer = new Offer();
			$this->_offer->setOffer($data['offer'],$data['promo']);
			return $this->_offer->_aditional;
		}

		protected function setName($first,$last) {
			$this->_firstname	=	$first;
			$this->_lastname	=	$last;
		}
		protected function setAddress($address1,$address2,$city,$state,$zip) {
			$this->_address1	=	$address1;
			$this->_address2	=	$address2;
			$this->_city		=	$city;
			$this->_state		=	$state;
			$this->_zip			=	$zip;
		}
		protected function setEmail($email) {
			$this->_email 		=	$email;
		}

		public function getOrderItemXML() {
			$xml 			=	"<orderItem>";

			$offerXML 		=		$this->_offer->getOfferXML();
			$xml 			.=		$offerXML;

			$xml 			.=		"<recipient>";
			$personXML 		= 			$this->getPersonXML();
			$xml 			.=			$personXML;

			$offerCodeXML	=			$this->_offer->getOfferCodeXML();
			$xml 			.=			$offerCodeXML;
			$xml 			.=		"</recipient>";
			$xml 			.=	"</orderItem>";

			return $xml;
		}

		public function getCustomerXML() {
			$xml 		=	"<customer>";
			$xml 		.=		"<accountNumber></accountNumber>";
			$personXML 	= 		$this->getPersonXML();
			$xml 		.=		$personXML;
			$xml 		.=	"</customer>";

			return $xml;
		}

		protected function getPersonXML() {
			$xml 	=		"<name>".$this->_firstname." ".$this->_lastname."</name>";
			$xml 	.=		"<firstName>".$this->_firstname."</firstName>";
			$xml 	.=		"<lastName>".$this->_lastname."</lastName>";
			$xml 	.=		"<address1>".$this->_address1."</address1>";
			$xml 	.=		"<address2>".$this->_address2."</address2>";
			$xml 	.=		"<city>".$this->_city."</city>";
			$xml 	.=		"<state>".$this->_state."</state>";
			$xml 	.=		"<zipCode>".$this->_zip."</zipCode>";
			$xml 	.=		"<country>United States</country>";
			$xml 	.=		"<email>".$this->_email."</email>";

			return $xml;
		}

		public function getFirstName() {
			return $this->_firstname;
		}

		public function getLastName() {
			return $this->_lastname;
		}

		public function getAddress1() {
			return $this->_address1;
		}

		public function getAddress2() {
			return $this->_address2;
		}
		
		public function getCity() {
			return $this->_city;
		}

		public function getState() {
			return $this->_state;
		}

		public function getZip() {
			return $this->_zip;
		}

		public function getEmail() {
			return $this->_email;
		}

		public function getOffer() {
			return $this->_offer;
		}
	}
