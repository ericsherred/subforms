<?php 

/**
* Promo Class: checks promo codes and holds promotional data
*/

class Promo
{
	protected $_id;
	protected $_code;
	protected $_discount;
	protected $_isActive;

	function __construct($promo)
	{
		$this->_isActive = $promo;
		$this->_code = $promo;
		try {
			$dbcon = new MyDB();
			$result = $dbcon->selectFrom('sub_promo_codes', $columns = null, $where = array('code_name' => $this->_code), $like = false, $orderby = "code_id", $direction = "DESC", $limit = null, $offset = null);
		} catch (Exception $e) {
			echo 'Database Error: ', $e;
		}
		if ($result['result'][0]['code_active'] == 1) {
			$this->_isActive = true;
			$this->_id = $result['result'][0]['code_id'];
			$this->_discount = $result['result'][0]['code_discount'];
		} else {
			$this->_isActive = false;
			$this->_id = -1;
			$this->_discount = 0;
		}
	}

	function getDiscount() {
		return $this->_discount / 100;
	}

	function getCode() {
		return $this->_code;
	}

	function isActive() {
		return $this->_isActive;
	}

	function getID() {
		return $this->_id;
	}
}



?>