<?php

	include('class.Person.inc');
	include('class.Payment.inc');
	include('class.MyDB.inc');

	/**
	* Order: defines, sets up and submits an order to CDS
	*/
	class Order {

		protected $_postURL;
		protected $_appID;
		protected $_pwd;
		protected $_people;
		protected $_payment;
		protected $_promotionKey;
		protected $_json;
		protected $_xmlString;
		protected $_internetNumber;

		public $apiResult;
		public $transaction;

		function __construct() {
			$this->_postURL		=	'https://service.mycdsglobal.com/ws/service/order/WSH';
			$this->_appID		=	'com.washingtonian.washingtonianmagazine.web';
			$this->_pwd			=	'3fe7c7664c8be3c9';
		}

		public function setOrder($data) {
			// For Person object
			$first		=	$data['firstname']; // Firsty Name Array
			$last		=	$data['lastname'];	// Last Name Array
			$address1	=	$data['address1'];	// Address1 Array
			$address2	=	$data['address2'];	// Address2 Array
			$city		=	$data['city'];		// City Array
			$state		=	$data['state'];		// State Array
			$zip		=	$data['zip'];		// Zip Array
			$email		=	$data['email'];		// Email array
			$offer		=	$data['subscribe_offer'];		// Initial Offer Value

			// For Promo object
			if (isset($data['promo_code'])) {
				$promo = $data['promo_code'];
			} else {
				$promo = null;
			}

			// For _Payment object
			$ccType		=	$data['payment_method'];		// Credit Card Type
			$cc 		=	$data['payment_cc'];			// Credit Card Number
			$expM		=	$data['payment_exp_month'];		// Credit Card Expitation Month
			$expY		=	$data['payment_exp_year'];		// Credit card Expiration Year

			// // For Order object
			$this->_promotionKey	=	$data['promotionkey'];

			if (isset($data['mailing_diff_recipient']) && $data['mailing_diff_recipient'] == '0') {
				$count = count($first) - 1;
				for ($i=0; $i < $count; $i++) { 
					$person		=	new Person();
					$p = array(
						'first' => $first[$i],
						'last' => $last[$i],
						'address1' => $address1[$i],
						'address2' => $address2[$i],
						'city' => $city[$i],
						'state' => $state[$i],
						'zip' => $zip[$i],
						'email' => $email[$i],
						'offer' => $offer,
						'promo' => $promo);
					$offer 		=	$person->setPerson($p);
					$this->_people[$i]	=	$person;
				}
			} else {
				for ($i=0; $i < count($first); $i++) { 
					$person		=	new Person();
					$p = array(
						'first' => $first[$i],
						'last' => $last[$i],
						'address1' => $address1[$i],
						'address2' => $address2[$i],
						'city' => $city[$i],
						'state' => $state[$i],
						'zip' => $zip[$i],
						'email' => $email[$i],
						'offer' => $offer,
						'promo' => $promo);
					$offer 		=	$person->setPerson($p);
					$this->_people[$i]	=	$person;
				}
			}

			$this->_payment = new Payment(array(
				'type' => $ccType,
				'number' => $cc,
				'expM' => $expM,
				'expY' => $expY));
		}

		public function setXML() {
			$offer = $this->_people[0]->getOffer();
			if ($offer->getID() >= 13 && $offer->getID() <= 15) {
				if ($offer->getProductCode() == 'C') {
					$promoKryXML	=	"<promotionKey>I14COL</promotionKey>";
					$this->_promotionKey = 'I14COL';
				} else {
					$promoKryXML	=	"<promotionKey>I14COLD</promotionKey>";
					$this->_promotionKey = 'I14COLD';
				}
			} else {
				$promoKryXML 	=	"<promotionKey>".$this->_promotionKey."</promotionKey>";
			}
			$customerXML	=	$this->_people[count($this->_people) - 1]->getCustomerXML();
			$paymentXML		=	$this->_payment->getPaymentXML();

			$orderItemXML = array();
			for ($i=0; $i < count($this->_people); $i++) { 
				$orderItemXML[$i] = $this->_people[$i]->getOrderItemXML();
			}

			$this->_xmlString		=	"<order>";
			$this->_internetNumber 	= 	time();
			$this->_xmlString		.=	"<internetNumber>".$this->_internetNumber."</internetNumber>";
			$this->_xmlString		.=	$promoKryXML;
			$this->_xmlString		.=	$customerXML;
			$this->_xmlString		.=	$paymentXML;

			for ($j=0; $j < count($orderItemXML); $j++) { 
				$this->_xmlString 	.=	$orderItemXML[$j];
			}
			$this->_xmlString		.=	"</order>";
		}

		public function submitOrder() {
/*			$xmlURL = $this->_postURL."?appID=".$this->_appID."&pwd=".$this->_pwd;
			$xml = '<?xml version="1.0" encoding="utf-8"?>'.$this->_xmlString;*/
			$xmlURL = 'https://service.mycdsglobal.com/ws/service/order/WSH?appId=com.washingtonian.washingtonianmagazine.web&pwd=3fe7c7664c8be3c9';
			$xml = $xml = '<?xml version="1.0" encoding="utf-8"?>'.$this->_xmlString;

			$ch = curl_init($xmlURL);
			
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$ch_result = curl_exec($ch);

			curl_close($ch);

			$simpleXml = simplexml_load_string($ch_result);
			$json = json_encode($simpleXml);
			$json = json_decode($json);

			$this->_json = $json;

			$this->apiResult = $json;

			$this->setTransactionResult($json);

		}

		protected function setTransactionResult($result) {
			$trans_info['isSuccess'] 			=	$result->isSuccess; 
			$trans_info['sentToOffline']		=	$result->sentToOffline;
			$trans_info['message']				=	$result->message;

			$this->transaction = $trans_info;
		}

		public function isSuccess() {
			$success = $this->transaction['isSuccess'];

			return $success;
		}

		public function submitToDB() {

			$order = $this->apiResult;

			$cds_returned_order = $order->order;

			$count = count($this->_people);

			$data['sub_internet_no']		=	$this->_internetNumber;
			$data['sub_promotionkey']		=	$this->_promotionKey;
			$data['sub_firstname']			=	$this->_people[$count-1]->getFirstName();
			$data['sub_lastname']			=	$this->_people[$count-1]->getLastName();
			$data['sub_address1']			=	$this->_people[$count-1]->getAddress1();
			$data['sub_address2']			=	$this->_people[$count-1]->getAddress2();
			$data['sub_city']				=	$this->_people[$count-1]->getCity();
			$data['sub_state']				=	$this->_people[$count-1]->getState();
			$data['sub_zipcode']			=	$this->_people[$count-1]->getZip();
			$data['sub_country']			=	'United States';
			$data['sub_email']				=	$this->_people[$count-1]->getEmail();
			$data['sub_payment_type']		=	$this->_payment->getCardType();
			$data['sub_payment_cc']			=	substr($this->_payment->getCardNum(), -4);
			$data['sub_payment_exp_mo']		=	$this->_payment->getExpMonth();
			$data['sub_payment_exp_yr']		=	$this->_payment->getExpYear();

			if (count($this->_people) == 1) {
				$data['sub_transaction_orders']	=		count($this->_people);
				$offer = $this->_people[0]->getOffer();
				$data['sub_subscription_value']	=		$offer->getPrice();
			}
			else {
				$data['sub_transaction_orders']	=	count($this->_people) - 1;
				$data['sub_subscription_value']	=	0;
				for ($i=0; $i < count($this->_people) - 1; $i++) { 
					$offer = $this->_people[$i]->getOffer();
					$data['sub_subscription_value']	= $offer->getPrice();
				}
			}
			$offer = $this->_people[0]->getOffer();
			$data['sub_subscription_term']		=		$offer->getTerm();
			$data['sub_type']					=		$offer->getProductCode();
			$data['sub_subscription_offer']		=		$offer->getProductCode();
			$data['sub_promo_code']				=		$offer->getPromo();

			if (empty($cds_returned_order->customer->accountNumber)) {
				$data['sub_cds_accountnumber'] 	= 		'NA';
			}
			else {
				$data['sub_cds_accountnumber']	=	    $cds_returned_order->customer->accountNumber;
			}

			$data['sub_cds_isTransSuccess']		=		$this->transaction['isSuccess'];
			$data['sub_cds_isSentOffline']		=		$this->transaction['sentToOffline'];
			$data['sub_cds_msg']				=		$this->transaction['message'];
			$data['sub_date_submitted']			=		$this->_internetNumber;
			$data['sub_xml_string_sent']		=		$this->_xmlString;
			$data['sub_xml_string_returned']	=		json_encode($order);

			try {
				$con = new MyDB();
				$query = $con -> insertInto("subscribes", $fields = $data);
				// return true;
			} catch (Exception $e) {
				echo 'There was an error: ',  $e->getMessage(), "\n";
			}

			$redirectURL = '../confirmation.php';

			$first = 'firstname='.urlencode($data['sub_firstname']);
			$last = 'lastname='.urlencode($data['sub_lastname']);
			$address1 = 'address1='.urlencode($data['sub_address1']);
			$address2 = 'address2='.urlencode($data['sub_address2']);
			$city = 'city='.urlencode($data['sub_city']);
			$state = 'state='.urlencode($data['sub_state']);
			$zip = 'zipcode='.urlencode($data['sub_zipcode']);
			$country = 'country='.urlencode($data['sub_country']);
			$email = 'email='.urlencode($data['sub_email']);
			$value = 'value='.urlencode($data['sub_subscription_value']);
			$amp = '&';
			$q = '?';

			$url = $redirectURL.$q.$first.$amp.$last.$amp.$address1.$amp.$address2.$amp.$city.$amp.$state.$amp.$zip.$amp.$country.$amp.$email.$amp.$value;

			//header("'Location: ".$url."'");
			print '<meta http-equiv="refresh" content="0;URL='.$url.'">';

		}

	}