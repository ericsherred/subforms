<?php
	include('inc/checkSession.inc');

?>
<!DOCTYPE html>
<html lang="">
	<head>
		<title>Search Subscriber Database</title>
		<meta charset=utf-8>
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" media="screen">
	</head>
	<body>
		<div class="container">
			<form action="query.php" method="POST" class="form-horizontal" role="form">
				<div class="form-group">
					<legend>Search Subscriber Database <span class="pull-right"><a href="logout.php">Logout</a></span></legend>
				</div>
				<div class="form-group">
					<label for="inputKey" class="col-sm-1 control-label">Key:</label>
					<div class="col-sm-2">
						<input type="text" name="key" id="inputKey" class="form-control" title="PromoKey">
					</div>
					<label for="inputEmail" class="col-sm-1 control-label">Email:</label>
					<div class="col-sm-2">
						<input type="text" name="email" id="inputEmail" class="form-control" title="Email">
					</div>
					<div class="checkbox col-sm-2">
						<label class="pull-right">
							<input type="hidden" name="online" value="0"><input type="checkbox" name="online" value="1">
							Only Processed
						</label>
					</div>
					<div class="col-sm-2">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	</body>
</html>