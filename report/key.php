<?php

	include('inc/class.MyDB.inc');
	include('inc/checkSession.inc');

	try {
		$con = new MyDB();
		// $query = $con -> selectFrom("subs_test", $columns = array('sub_id','sub_promotionkey','sub_firstname'), $where = null, $like = false, $orderby = "sub_id", $direction = "DESC", $limit = null, $offset = null);
		$columns = array(
			'key_id',
			'key_val',
			'key_desc',
			'form_type',
			'form_name',
			'key_exp'
		);

		$where = NULL;

		$query = $con->selectFrom($table = "sub_key", $columns, $where, $like = false, $orderby = "key_id", $direction = "DESC", $limit = NULL, $offset = null);

		// $formType = $con->selectFrom($table = "form_type", $columns = null, $where = null, $like = false, $orderby = "form_type", $direction = "ASC", $limit = NULL, $offset = null);

		// $forms = $con->selectFrom($table = "form", $columns = null, $where = null, $like = false, $orderby = "type_id", $direction = "ASC", $limit = NULL, $offset = null);

		// var_dump($query);
	} catch (Exception $e) {
		echo "<h1>There was a DB error</h1>";
	}

 ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Results - Search Subscriber Database</title>
		<meta charset=utf-8>
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" media="screen">
	</head>
	<body>
		<?php include('templates/nav.php'); ?>
		<div class="container">

			<form action="inc/process/addCode.php" method="POST" class="form-horizontal" role="form">
				<a data-toggle="collapse" href="#add"><legend>Add Form Key +</legend></a>
				<div class="collapse" id="add">
					<div class="form-group">
						<div class="col-sm-4">
							<input type="text" name="code" id="inputCode" class="form-control" required="required" placeholder="New Code">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<input type="text" name="desc" id="inputDesc" class="form-control" required="required" placeholder="Code Description">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<input type="number" name="disc" id="inputDisc" class="form-control" required="required" placeholder="Percent Discount" min="0" max="100">
						</div>
					</div>
					<div>
							<button type="submit" class="btn btn-primary">Add Code</button>
					</div>
				</div>
			</form>
			
			<div class="clearfix"></div>
			<h2><?php echo $query['num']; ?> results returned</h2>
			<div class="table-responsive">
				<table class="table table-hover table-striped table-condensed">
					<thead>
						<tr>
							<th>ID</th>
							<th>Kay</th>
							<th>Description</th>
							<th>Form Type</th>
							<th>Form Name</th>
							<th>Expires</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							for ($i=0; $i < $query['num']; $i++) { 
								echo '<tr>';
									echo '<td>';
										echo $query['result'][$i]['key_id'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['key_val'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['key_desc'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['form_type'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['form_name'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['key_exp'];
									echo '</td>';
								echo '</tr>';
							}
						?>
					</tbody>
				</table>
			</div>
			<div>
				<pre><?php echo $query['sql'] ?></pre>
			</div>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	</body>
</html>