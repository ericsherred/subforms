<?php 
	require 'inc/class.MyDB.inc';
	
	$data = $_POST;
	$user = $data['user'];

	try {
		$c = new MyDB();
	} catch (Exception $e) {
		echo $e;
	}

	$r = $c->selectFrom('sub_admin',array('user'),array('user' => $user));

	if(!isset($r) || $r['num'] == 0) {
		header('Location:reset.php?session=userfail');
	}

	if($_GET && isset($_GET['session'])) {
		$query = $_GET['session'];
	}
 ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Password Reset</title>
		<meta charset=utf-8>
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="css/styles.css">
	</head>
	<body>

		<div class="container">

			<h1 class="text-center">Washingtonian Subscription Admin
			<br><small>Password Reset</small>
			</h1>

	    	<form method="POST" action="inc/process/password.php" class="form-signin" role="form">

	    		<?php 

					if (isset($query)) {
						switch ($query) {
							case 'match':
								echo '<div class="alert alert-danger">Your passwords did not match. Please try again.</div>';
								break;
							/*default:
								echo '<div class="alert alert-info">Please sign in.</div>';
								break;*/
						}
					}
				?>

	    		<input type="hidden" name="user" id="inputUser" class="form-control" value="<?php echo $user; ?>">

	        	<!-- <h2 class="form-signin-heading">Please sign in</h2> -->
	        	<input type="password" class="form-control" placeholder="New Password" name="pass" required>
	        	<input type="password" class="form-control" placeholder="Confirm Password" name="con_pass" required>
				<!--<label class="checkbox">
	        		<input type="checkbox" value="remember-me"> Remember me
	        	</label> -->
	        	<button class="btn btn-lg btn-primary btn-block" type="submit">Reset</button>
	      	</form>

    	</div> <!-- /container -->

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	</body>
</html>