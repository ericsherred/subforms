<?php

	include('inc/class.MyDB.inc');
	include('inc/checkSession.inc');

	try {
		$con = new MyDB();
		// $query = $con -> selectFrom("subs_test", $columns = array('sub_id','sub_promotionkey','sub_firstname'), $where = null, $like = false, $orderby = "sub_id", $direction = "DESC", $limit = null, $offset = null);
		$columns = array(
			'code_id',
			'code_name',
			'code_desc',
			'code_discount',
			'code_active'
		);

		$where = NULL;

		$query = $con->selectFrom($table = "sub_promo_codes", $columns, $where, $like = false, $orderby = "code_id", $direction = "DESC", $limit = NULL, $offset = null);
		// var_dump($query);
	} catch (Exception $e) {
		echo "<h1>There was a DB error</h1>";
	}

 ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Results - Search Subscriber Database</title>
		<meta charset=utf-8>
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" media="screen">
	</head>
	<body>
		<?php include('templates/nav.php'); ?>
		<div class="container">

			<form action="inc/process/addCode.php" method="POST" class="form-horizontal" role="form">
				<a data-toggle="collapse" href="#add"><legend>Add Promotion Code +</legend></a>
				<div class="collapse" id="add">
					<div class="form-group">
						<div class="col-sm-4">
							<input type="text" name="code" id="inputCode" class="form-control" required="required" placeholder="New Code">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<input type="text" name="desc" id="inputDesc" class="form-control" required="required" placeholder="Code Description">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<input type="number" name="disc" id="inputDisc" class="form-control" required="required" placeholder="Percent Discount" min="0" max="100">
						</div>
					</div>
					<div>
							<button type="submit" class="btn btn-primary">Add Code</button>
					</div>
				</div>
			</form>
			<form action="inc/process/updateCodes.php" method="POST" class="form-horizontal" role="form">	
				<a data-toggle="collapse" href="#update"><legend>Edit Promotion Codes +</legend></a>
				<div class="collapse" id="update">
					<div class="form-group">
						<div class="select col-sm-3">
							Select Code To Deactivate
							<select multiple name="deactivate[]" id="inputDeactivate" class="form-control">
								<?php 
									for ($j=0; $j < $query['num']; $j++) { 
										if ($query['result'][$j]['code_active'] == 1)
											echo '<option value="', $query['result'][$j]['code_id'],'">', $query['result'][$j]['code_name'], '</option>';
									}
								
								 ?>
							</select>
						</div>
						<div class="select col-sm-3">
							Select Code To Activate
							<select multiple name="activate[]" id="inputActivate" class="form-control">
								<?php 
									for ($j=0; $j < $query['num']; $j++) { 
										if ($query['result'][$j]['code_active'] == 0)
											echo '<option value="', $query['result'][$j]['code_id'],'">', $query['result'][$j]['code_name'], '</option>';
									}
								
								 ?>
							</select>
						</div>
					</div>
					<div>
						<button type="submit" class="btn btn-primary">Update Codes</button>
					</div>
				</div>
			</form> 
			
			<div class="clearfix"></div>
			<h2><?php echo $query['num']; ?> results returned</h2>
			<div class="table-responsive">
				<table class="table table-hover table-striped table-condensed">
					<thead>
						<tr>
							<th>ID</th>
							<th>Code</th>
							<th>Description</th>
							<th>Discount</th>
							<!-- <th>Accont</th> -->
							<th>Active?</th>
						</tr>
					</thead>
					<tbody>
						<?php 
							for ($i=0; $i < $query['num']; $i++) { 
								echo '<tr>';
									echo '<td>';
										echo $query['result'][$i]['code_id'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['code_name'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['code_desc'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['code_discount'], '%';
									echo '</td>';
									echo '<td>';
										if ($query['result'][$i]['code_active'] == 1) {
											echo 'Yes';
										}
										if ($query['result'][$i]['code_active'] == 0) {
											echo 'No';
										}
									echo '</td>';
								echo '</tr>';
							}
						?>
					</tbody>
				</table>
			</div>
			<div>
				<pre><?php echo $query['sql'] ?></pre>
			</div>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	</body>
</html>