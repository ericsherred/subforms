<nav class="navbar navbar-default" role="navigation">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="home.php">Washingtonian Subscriptions Admin</a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav">
			<?php
			echo '<li', ($_SERVER['REQUEST_URI'] == '/report/home.php' ? ' class="active"' : '') ,'>';
				echo '<a href="home.php">Home</a>';
			echo '</li>';
			?>
			<?php
			echo '<li', ($_SERVER['REQUEST_URI'] == '/report/query.php' ? ' class="active"' : '') ,'>';
				echo '<a href="query.php">Search</a>';
			echo '</li>';
			?>
			<?php
			// echo '<li', ($_SERVER['REQUEST_URI'] == '/report/codes.php' ? ' class="active"' : '') ,'>';
			// 	echo '<a href="codes.php">PromoCodes</a>';
			// echo '</li>';
			?>
			<?php
			// echo '<li', ($_SERVER['REQUEST_URI'] == '/report/key.php' ? ' class="active"' : '') ,'>';
			// 	echo '<a href="key.php">Key</a>';
			// echo '</li>';
			?>
		</ul>
		<ul class="nav navbar-nav navbar-right">
			<li><a href="logout.php">Logout</a></li>
		</ul>
	</div><!-- /.navbar-collapse -->
</nav>