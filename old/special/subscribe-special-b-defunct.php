<?php 
/* 	
	SPECIAL FORM A (custom layout/form-- WITH NO VARIATION as in control form-A). 
*/
require_once('../_includes/scripts.inc');
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
<link type="text/css" rel="stylesheet" href="css/custom-form-v2.css" />
<script type="text/javascript" src="js/jquery.validate.js"></script>

<script src="http://www.washingtonian.com/js/jquery-1.9.1.min.js"></script>

<script>

$(document).ready(function() {

	$('#mailing-address').hide();

	$('#show-mailing').click(function() {		
		if( $('#show-mailing').is(':checked') ) {
			$('#mailing-address').fadeIn('fast');
			$('#sub-data > div.left').animate({'height':'660px'});
		}
	
		else {
			$('#mailing-address').fadeOut('fast');
			$('#sub-data > div.left').animate({'height':'360px'});
		}	
	});
	
	$('#sub-options input[type=radio]').click(function() {
		if( $('input[type=radio].all').is(':checked') ) {
			$(this).parent().parent().addClass('picked');
			$(this).parent().parent().siblings().removeClass('picked');
		}
		else if( $('input[type=radio].digital').is(':checked') ) {
			$(this).parent().parent().addClass('picked');
			$(this).parent().parent().siblings().removeClass('picked');
		}
	});

});

</script>

<title>Washingtonian Subscription</title>
</head>

<body>
<header>

<div id="sub-header">
<img src="images/Wash-Logo-White.png" class="main-logo" />
<span class="bar">
<h2>Subscribe to the Magazine Washington Lives By</h2>
</span>
</div> <!-- #sub-header -->
</header>


<div id="subframe">
<form id="form" method="post" action="<?php print $form_action_location;?>" id="specialForm">
<div id="sub-options" class="clearfix">
<h3>Select An Offer</h3>
<div class="left default">
<img src="images/small-2013_Aug.jpg" class="magazine" />
<h4>All Inclusive</h4>
<h5>Print + iPad + Desktop</h5>
<p>Our best offer! Select your term below.*</p>
<span><input type="radio" value="1" name="subscribe_offer" class="all" required/> <label>1 year for $29.95 (12 issues)</label></span>
<span><input type="radio" value="2" name="subscribe_offer" class="all" /> <label>2 years for $49.95 (24 issues)</label></span>
<span><input type="radio" value="3" name="subscribe_offer" class="all" /> <label>Outside DC, MD, & VA: 1 year for $39.95</label></span>
<span><input type="radio" value="4" name="subscribe_offer" class="all" /> <label>Outside DC, MD, & VA: 2 years for $69.95</label></span>
</div>

<div class="right default">
<img src="images/aug-tablet.png" />
<h4>Digital Only</h4>
<h5>iPad + Desktop</h5>
<p>Read the magazine on your iPad or computer&mdash;anytime!</p>
<span><input type="radio" value="5" name="subscribe_offer" class="digital" /> <label>1 year for $19.99/year*</label></span>
</div>
</div> <!-- #sub-options -->

<div id="sub-extras">

</div> <!-- #sub-extras -->

<div id="sub-data" class="clearfix">
<h3>Billing Information</h3>
<div class="left default">
<span><label>First Name</label> <input type="text" name="firstname" required/></span>
<span><label>Last Name</label> <input type="text" name="lastname" required/></span>
<span><label>Address</label> <input type="text" name="address1" required/></span>
<span><label>Address 2</label> <input type="text" name="address2"/></span>
<span><label>City</label> <input type="text" name="city" required/></span>
<span><label>State</label> 

<select name="state" required>
            <option value="" selected="SELECTED">Select A State</option>
            <option value="AA" >APO/FPO-Americas</option><option value="AE" >APO/FPO-Middle East</option><option value="AP" >APO/FPO-Pacific</option><option value="AK" >Alaska</option><option value="AL" >Alabama</option><option value="AR" >Arkansas</option><option value="AS" >American Samoa</option><option value="AZ" >Arizona</option><option value="CA" >California</option><option value="CO" >Colorado</option><option value="CT" >Connecticut</option><option value="DC" >District of Columbia</option><option value="DE" >Delaware</option><option value="FL" >Florida</option><option value="GA" >Georgia</option><option value="GU" >Guam</option><option value="HI" >Hawaii</option><option value="IA" >Iowa</option><option value="ID" >Idaho</option><option value="IL" >Illinois</option><option value="IN" >Indiana</option><option value="KS" >Kansas</option><option value="KY" >Kentucky</option><option value="LA" >Louisiana</option><option value="MA" >Massachusetts</option><option value="MD" >Maryland</option><option value="ME" >Maine</option><option value="MI" >Michigan</option><option value="MN" >Minnesota</option><option value="MO" >Missouri</option><option value="MS" >Mississippi</option><option value="MT" >Montana</option><option value="NC" >North Carolina</option><option value="ND" >North Dakota</option><option value="NE" >Nebraska</option><option value="NH" >New Hampshire</option><option value="NJ" >New Jersey</option><option value="NM" >New Mexico</option><option value="NV" >Nevada</option><option value="NY" >New York</option><option value="OH" >Ohio</option><option value="OK" >Oklahoma</option><option value="OR" >Oregon</option><option value="PA" >Pennsylvania</option><option value="PR" >Puerto Rico</option><option value="RI" >Rhode Island</option><option value="SC" >South Carolina</option><option value="SD" >South Dakota</option><option value="TN" >Tennessee</option><option value="TX" >Texas</option><option value="UT" >Utah</option><option value="VA" >Virginia</option><option value="VT" >Vermont</option><option value="WA" >Washington</option><option value="WI" >Wisconsin</option><option value="WV" >West Virginia</option><option value="WY" >Wyoming</option>
          </select> 
          
          
        <label>ZIP</label> <input type="text" name="zip" style="width:50px;" required/></span>
        
<span><label>Email</label> <input type="email" name="email" required/></span>

<span><input type="checkbox" name="show-mailing" id="show-mailing" /> <em>Click here if mailing address differs from billing address</em></span>


<div id="mailing-address" class="left">
<h3>Mailing Address</h3>
<span><label>Name</label> <input type="text" name="mailing_firstname" /></span>
<span><label>Address</label> <input type="text" name="mailing_address1" /></span>
<span><label>Address 2</label> <input type="text" name="mailing_address2" /></span>
<span><label>City</label> <input type="text" name="mailing_city" /></span>
<span><label>State</label> 

<select name="mailing_state">
            <option value="" selected="SELECTED">Select A State</option>
            <option value="AA" >APO/FPO-Americas</option><option value="AE" >APO/FPO-Middle East</option><option value="AP" >APO/FPO-Pacific</option><option value="AK" >Alaska</option><option value="AL" >Alabama</option><option value="AR" >Arkansas</option><option value="AS" >American Samoa</option><option value="AZ" >Arizona</option><option value="CA" >California</option><option value="CO" >Colorado</option><option value="CT" >Connecticut</option><option value="DC" >District of Columbia</option><option value="DE" >Delaware</option><option value="FL" >Florida</option><option value="GA" >Georgia</option><option value="GU" >Guam</option><option value="HI" >Hawaii</option><option value="IA" >Iowa</option><option value="ID" >Idaho</option><option value="IL" >Illinois</option><option value="IN" >Indiana</option><option value="KS" >Kansas</option><option value="KY" >Kentucky</option><option value="LA" >Louisiana</option><option value="MA" >Massachusetts</option><option value="MD" >Maryland</option><option value="ME" >Maine</option><option value="MI" >Michigan</option><option value="MN" >Minnesota</option><option value="MO" >Missouri</option><option value="MS" >Mississippi</option><option value="MT" >Montana</option><option value="NC" >North Carolina</option><option value="ND" >North Dakota</option><option value="NE" >Nebraska</option><option value="NH" >New Hampshire</option><option value="NJ" >New Jersey</option><option value="NM" >New Mexico</option><option value="NV" >Nevada</option><option value="NY" >New York</option><option value="OH" >Ohio</option><option value="OK" >Oklahoma</option><option value="OR" >Oregon</option><option value="PA" >Pennsylvania</option><option value="PR" >Puerto Rico</option><option value="RI" >Rhode Island</option><option value="SC" >South Carolina</option><option value="SD" >South Dakota</option><option value="TN" >Tennessee</option><option value="TX" >Texas</option><option value="UT" >Utah</option><option value="VA" >Virginia</option><option value="VT" >Vermont</option><option value="WA" >Washington</option><option value="WI" >Wisconsin</option><option value="WV" >West Virginia</option><option value="WY" >Wyoming</option>
          </select></span> 
          
          
        <span><label>ZIP</label> <input type="text" style="width:50px;" name="mailing_zip" /></span>
</div>

</div>

<div class="right default">
<h4>Payment Information <img src="images/credit-cards.png" /></h4>
<img src="images/secure-trans.png" width="126" class="secure" />
<span class="clearfix"><label>Payment Method</label> <select name="payment_method" id="cc_cards" required>
                <option value="" selected="selected">Select a payment method</option>
                <option value="MC">MasterCard</option>
                <option value="VISA">Visa</option>
				<option value="AMEX">American Express</option>
              </select>
</span>
<span class="clearfix"><label>Credit Card Number</label> <input type="creditcard" name="payment_cc" required/></span>
<span class="clearfix"><label>Experation Date</label> 
<select name="payment_exp_month" style="width:90px;" required>
              <option value="" selected="selected">Month</option>
              <option value="01"     >01</option>
              <option value="02"     >02</option>
              <option value="03"     >03</option>
              <option value="04"     >04</option>
              <option value="05"     >05</option>
              <option value="06"     >06</option>
              <option value="07"     >07</option>
              <option value="08"     >08</option>
              <option value="09"     >09</option>
              <option value="10"     >10</option>
              <option value="11"     >11</option>
              <option value="12"     >12</option>
		</select>
        
        <select name="payment_exp_year" SIZE="1" class="input_98" style="width:80px; margin-left:10px;" required>
              <option value="" selected="selected">Year</option>
              <option value="13" >2013</option><option value="14" >2014</option><option value="15" >2015</option><option value="16" >2016</option><option value="17" >2017</option><option value="18" >2018</option><option value="19" >2019</option><option value="20" >2020</option><option value="21" >2021</option><option value="22" >2022</option><option value="23" >2023</option>
            </select>
</span>
</div>
</div>

<p class="renewal">
<strong>* Automatic Renewal Program:</strong> Your subscription will be automatically renewed unless you tell us to stop. Before the start of each renewal, you will be sent a reminder notice stating the term and rate then in effect. If you do nothing, your credit/debit card will be charged or you will be sent an invoice for your subscription. You may cancel at any time during subscription and receive a full refund for unmailed issues. 
</p>
<INPUT TYPE="HIDDEN" name="promotionkey" value="<?php print $promotionKey['special']['b']; ?>"/> 
<input type="submit" id="submit" value="subscribe" />
</form> <!-- #form -->

<div id="sub-contacts">
<p>Login to your <a href="https://w1.buysub.com/pubs/WH/WSH/Combo3Entry.jsp?cds_page_id=22265">Subscriber Services</a> account to update your subscription settings.<br /> Or call 202-331-0715 to speak with a member of our circulation team.</p>
</div> <!-- #sub-contacts -->

</div> <!-- #subframe -->
<script>
$("#specialForm").validate();
</script>
</body>
</html>