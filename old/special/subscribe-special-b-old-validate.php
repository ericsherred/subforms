<?php 
/* 	
	SPECIAL FORM A (custom layout/form-- WITH NO VARIATION as in control form-A). 
*/

require_once('../_includes/scripts.inc');

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<link rel="stylesheet" type="text/css" href="css/special-form-b.css" />
<link rel="stylesheet" href="fonts/fontello_V7/css/fontello.css">
    <link rel="stylesheet" href="fonts/fontello_V7/css/animation.css"><!--[if IE 7]>
    <link rel="stylesheet" href="fonts/fontello_V7/css/fontello-ie7.css"><![endif]-->

<title>Subscribe | Washingtonian</title>
</head>

<body>
<div id="sub-wrapper" class="clearfix">
<div id="top-sub-bar">
<h4>Magazine Subscription Form <i class="icon-down-dir"></i></h4>
</div> <!-- #top-sub-bar -->

<div id="cntr">
<img src="images/Wash-Logo-Blue.png" height="66" id="wash-logo" />
<span id="savings">Save<br /> <span>up to</span> <em>50%</em></span>


<p style="clear:both;"><span style="color:#9A1226;">Fill in the form below</span> to subscribe to Washingtonian for up to 50% OFF what others pay on the newsstand &mdash; that's like getting 6 FREE issues!</p>

<form method="post" action="<?php print $form_action_location;?>" id="specialForm">

<ul class="form-fields left">
<li>
<label>First Name:</label> <input type="text" name="firstname" required />
</li>
<li>
<label>Last Name:</label> <input type="text" name="lastname" required />
</li>
<li>
<label>Address:</label> <input type="text" name="address1" required />
</li>
<li>
<label>Address 2:</label> <input type="text" name="address2" />
</li>
</ul>

<ul class="form-fields right">
<li>
<label>City:</label> <input type="text" name="city" required="required" />
</li>
<li>
<label>State:</label> 

<select name="state">
            <option value="" selected="SELECTED">Select A State</option>
            <option value="AA" >APO/FPO-Americas</option><option value="AE" >APO/FPO-Middle East</option><option value="AP" >APO/FPO-Pacific</option><option value="AK" >Alaska</option><option value="AL" >Alabama</option><option value="AR" >Arkansas</option><option value="AS" >American Samoa</option><option value="AZ" >Arizona</option><option value="CA" >California</option><option value="CO" >Colorado</option><option value="CT" >Connecticut</option><option value="DC" >District of Columbia</option><option value="DE" >Delaware</option><option value="FL" >Florida</option><option value="GA" >Georgia</option><option value="GU" >Guam</option><option value="HI" >Hawaii</option><option value="IA" >Iowa</option><option value="ID" >Idaho</option><option value="IL" >Illinois</option><option value="IN" >Indiana</option><option value="KS" >Kansas</option><option value="KY" >Kentucky</option><option value="LA" >Louisiana</option><option value="MA" >Massachusetts</option><option value="MD" >Maryland</option><option value="ME" >Maine</option><option value="MI" >Michigan</option><option value="MN" >Minnesota</option><option value="MO" >Missouri</option><option value="MS" >Mississippi</option><option value="MT" >Montana</option><option value="NC" >North Carolina</option><option value="ND" >North Dakota</option><option value="NE" >Nebraska</option><option value="NH" >New Hampshire</option><option value="NJ" >New Jersey</option><option value="NM" >New Mexico</option><option value="NV" >Nevada</option><option value="NY" >New York</option><option value="OH" >Ohio</option><option value="OK" >Oklahoma</option><option value="OR" >Oregon</option><option value="PA" >Pennsylvania</option><option value="PR" >Puerto Rico</option><option value="RI" >Rhode Island</option><option value="SC" >South Carolina</option><option value="SD" >South Dakota</option><option value="TN" >Tennessee</option><option value="TX" >Texas</option><option value="UT" >Utah</option><option value="VA" >Virginia</option><option value="VT" >Vermont</option><option value="WA" >Washington</option><option value="WI" >Wisconsin</option><option value="WV" >West Virginia</option><option value="WY" >Wyoming</option>
          </select> 
</li>
<li>          
 <label>Zip:</label> <input type="text" name="zip" style="width:50px;" required="required" />
         
</li>
<li>
<label>Email:</label> <input type="text" name="email" required="required" />
</li>

</ul>

<div class="term">
<img src="images/2013_Sep_small.jpg" />
<h4>Select Term:</h4>
<p class="fine-print">Each term includes the features of the <strong>Automatic Renewal Program*</strong></p>
<ul class="price-options left">
<li>
<input type="radio" value="1" name="subscribe_offer" class="all" required/> <label><strong>Print + Digital &mdash;</strong> 1 year for $29.95 (12 issues)</label>
</li>
<li>
<input type="radio" value="2" name="subscribe_offer" class="all" /> <label><strong>Print + Digital &mdash;</strong> 2 years for $49.95 (24 issues)</label>
</li>
<li>
<input type="radio" value="5" name="subscribe_offer" class="digital" /> <label><strong>Digital Only &mdash;</strong> 1 year for $19.99/year*</label>
</li>
</ul>
</div>

<div class="payment left">
<h4>Select Secure Payment Method</h4>
<div class="payment-inner">
<ul>
<li>
<label>Choose Option</label> <select name="payment_method" id="cc_cards">
                <option value="" selected="selected" required>Select payment method</option>
                <option value="mastercard">MasterCard</option>
                <option value="visa">Visa</option>
				<option value="amex">American Express</option>
              </select>
              </li>
<li>
<label>Credit Card Number</label> <input type="text" name="payment_cc" required />
</li>
<li>
<label>Expiration Date</label> 
<select name="payment_exp_month" style="width:90px;">
              <option value="" selected="selected" required>Month</option>
              <option value="01">01</option>
              <option value="02">02</option>
              <option value="03">03</option>
              <option value="04">04</option>
              <option value="05">05</option>
              <option value="06">06</option>
              <option value="07">07</option>
              <option value="08">08</option>
              <option value="09">09</option>
              <option value="10">10</option>
              <option value="11">11</option>
              <option value="12">12</option>
		</select>
        
        <select name="payment_exp_year" SIZE="1" class="input_98" style="width:80px; margin-left:6px;">
              <option value="" selected="selected" required>Year</option>
              <option value="13" >2013</option><option value="14" >2014</option><option value="15" >2015</option><option value="16" >2016</option><option value="17" >2017</option><option value="18" >2018</option><option value="19" >2019</option><option value="20" >2020</option><option value="21" >2021</option><option value="22" >2022</option><option value="23" >2023</option>
            </select>
</li>
</ul>
</div> <!-- payment-inner -->
<img src="images/credit-cards.png" /> <img src="images/secure-trans.png" width="126" class="secure" />
</div> <!-- .payment -->

<p class="fine-print" style="clear:both; padding-top:18px;"><strong>*Automatic Renewal Program:</strong> Your subscription will be automatically renewed unless you tell us to stop. Before the start of each renewal, you will be sent a reminder notice stating the term and rate then in effect. If you do nothing, your credit/debit card will be charged or you will be sent an invoice for your subscription. You may cancel at any time during subscription and receive a full refund for unmailed issues.</p>

<!-- CHANGE PROMOTION KEY -->
<INPUT TYPE="HIDDEN" name="promotionkey" value="<?php print $promotionKey['special']['b']; ?>"/> 

<input type="submit" id="submit" value="subscribe" />

</form>

</div> <!-- #cntr -->

<div id="side">
</div> <!-- #side -->

</div> <!-- #sub-wrapper -->

 <div id="footer">
 <img src="images/Wash-Logo-Blue.png" height="20" />
 <p class="fine-print" style="margin-top:5px;">&copy;2013 Washington Magazine Company. 1828 L St. NW #200, Washington, DC 20036</p>
 <p class="fine-print" style="margin-top:8px;"><a style="color:#085899;" href="http://www.washingtonian.com/privacy-policy/">Privacy Policy</a> &nbsp; | &nbsp; <a style="color:#085899;" href="http://www.washingtonian.com/contact-us/">Contact Us</a></p>
 </div> <!-- footer -->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1860879-1']);
  _gaq.push(['_setDomainName', 'washingtonian.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

//Bad Robot Detection

	var category = 'trafficQuality';
	var dimension = 'botDetection';
	var human_events = ['onkeydown','onmousemove'];

	if ( navigator.appName == 'Microsoft Internet Explorer' && !document.referrer) {
		for(var i = 0; i < human_events.length; i++){
			document.attachEvent(human_events[i], ourEventPushOnce);
		}
	}else{
		_gaq.push( [ '_trackEvent', category, dimension, 'botExcluded', 1, true ] );
	}

	function ourEventPushOnce(ev) {

		_gaq.push( [ '_trackEvent', category, dimension, 'on' + ev.type, 1, true ] );

		for(var i = 0; i < human_events.length; i++){
			document.detachEvent(human_events[i], ourEventPushOnce);
		}

	} // end ourEventPushOnce()

	//End Bad Robot Detection

</script>

</body>
</html>