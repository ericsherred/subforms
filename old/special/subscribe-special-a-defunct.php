<?php 
/* 	
	SPECIAL FORM A (custom layout/form-- WITH NO VARIATION as in control form-A). 
*/

require_once('../_includes/scripts.inc');

?>

<!DOCTYPE HTML> 
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<title>Subscribe to Washingtonian Magazine</title>
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/magazine-subscribe.js"></script>
	<link type="text/css" href="css/styles.css" rel="stylesheet"/>
</head>

<body>
<div id="subpage-wrapper">
	<div id="subpage-topbar"></div>
	<div id="subpage-content-wrapper">
		<div id="subpage-col-left">
			<img src="images/washingtonian-logo.png"/>
			<h1>Subscribe now and save 50% off newsstand price</h1>
			<form method="post" action="<?php print $form_action_location;?>" id="specialForm">
			<div id="subpage-form">
				<h2>Select An Offer</h2>
				<div class="subpage-offer">
					<img src="images/cover-offer-magazine.png">
					<div class="subpage-offer-details">
						<h3><span class="subpage-offer-number">1.</span> All Inclusive</h3>
						<span class="subpage-offer-title">Print + iPad + Desktop Edition</span> <br/>
						<div class="subpage-offer-options">
							<em>Select Your Term*</em> <br/>
							<input type="radio" value="1" name="subscribe_offer" required/>DC, MD, & VA: 1 year (12 issues) for $29.95 <br/>
							<input type="radio" value="2" name="subscribe_offer"/>DC, MD, & VA: 2 years (24 issues) for $49.95 <br/>
							<input type="radio" value="3" name="subscribe_offer"/>Outside DC, MD, & VA: 1 year (12 issues) for $39.95 <br/>
							<input type="radio" value="4" name="subscribe_offer"/>Outside DC, MD, & VA: 2 year (24 issues) for $69.95
						</div>
					</div>
				</div>
				<div class="subpage-offer">
					<img src="images/cover-offer-digital.png">
					<div class="subpage-offer-details">
						<h3><span class="subpage-offer-number">2.</span> Digital Only</h3>
						<span class="subpage-offer-title">iPad + Desktop Edition</span> <br/>
						<div class="subpage-offer-options">
							<em>Read the magazine on your iPad or Computer, anytime!</em><br/>
							<input type="radio" value="5" name="subscribe_offer"/>1 year for $19.99* <br/>
						</div>
						<div class="subpage-offer-disclaimer">
							*Each subscription offer enrolls you in our <a href="#">automatic renewal program</a>.
						</div>
					</div>
				</div>
				
				<div class="subpage-billing">
					<h2>Billing Information</h2>
					<div class="subpage-form-item">
						<span class="subpage-form-label">First Name</span>
						<input class="subpage-form-field" type="text" name="firstname" value="" required/>
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">Last Name</span>
						<input class="subpage-form-field" type="text" name="lastname" value="" required/>
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">Address</span>
						<input class="subpage-form-field" type="text" name="address1" value="" required/>
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">&nbsp;</span>
						<input class="subpage-form-field" type="text" name="address2" value=""/>
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">City</span>
						<input class="subpage-form-field" type="text" name="city" value="" required />
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">State</span>
				
				          <SELECT name="state" SIZE="1" class="input_200 subpage-form-field" required>
				            <option value="" SELECTED SELECTED>Select A State</option>
				            <option value="AA"  >APO/FPO-Americas</option><option value="AE"  >APO/FPO-Middle East</option><option value="AP"  >APO/FPO-Pacific</option><option value="AK"  >Alaska</option><option value="AL"  >Alabama</option><option value="AR"  >Arkansas</option><option value="AS"  >American Samoa</option><option value="AZ"  >Arizona</option><option value="CA"  >California</option><option value="CO"  >Colorado</option><option value="CT"  >Connecticut</option><option value="DC"  >District of Columbia</option><option value="DE"  >Delaware</option><option value="FL"  >Florida</option><option value="GA"  >Georgia</option><option value="GU"  >Guam</option><option value="HI"  >Hawaii</option><option value="IA"  >Iowa</option><option value="ID"  >Idaho</option><option value="IL"  >Illinois</option><option value="IN"  >Indiana</option><option value="KS"  >Kansas</option><option value="KY"  >Kentucky</option><option value="LA"  >Louisiana</option><option value="MA"  >Massachusetts</option><option value="MD"  >Maryland</option><option value="ME"  >Maine</option><option value="MI"  >Michigan</option><option value="MN"  >Minnesota</option><option value="MO"  >Missouri</option><option value="MS"  >Mississippi</option><option value="MT"  >Montana</option><option value="NC"  >North Carolina</option><option value="ND"  >North Dakota</option><option value="NE"  >Nebraska</option><option value="NH"  >New Hampshire</option><option value="NJ"  >New Jersey</option><option value="NM"  >New Mexico</option><option value="NV"  >Nevada</option><option value="NY"  >New York</option><option value="OH"  >Ohio</option><option value="OK"  >Oklahoma</option><option value="OR"  >Oregon</option><option value="PA"  >Pennsylvania</option><option value="PR"  >Puerto Rico</option><option value="RI"  >Rhode Island</option><option value="SC"  >South Carolina</option><option value="SD"  >South Dakota</option><option value="TN"  >Tennessee</option><option value="TX"  >Texas</option><option value="UT"  >Utah</option><option value="VA"  >Virginia</option><option value="VT"  >Vermont</option><option value="WA"  >Washington</option><option value="WI"  >Wisconsin</option><option value="WV"  >West Virginia</option><option value="WY"  >Wyoming</option></select>
						
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">ZIP</span>
						<input type="text" name="zip" value="" class="subpage-form-field" required/>
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">Email</span>
						<input type="email" name="email" value="" class="subpage-form-field" required />
					</div>
					<div class="subpage-form-item">
						<input type="checkbox" name="mailing_diff_recipient" id="hasDiffMailing"/> Check if mailing address differs from billing address
					</div>
				</div>
				
				<div class="subpage-billing-mailing">
					<h2>Mailing Address</h2>
					<div class="subpage-form-item">
						<span class="subpage-form-label">Name</span>
						<input class="subpage-form-field" type="text" name="mailing_firstname" value=""/>
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">Address</span>
						<input class="subpage-form-field" type="text" name="mailing_address1" value=""/>
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">&nbsp;</span>
						<input class="subpage-form-field" type="text" name="mailing_address2" value=""/>
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">City</span>
						<input class="subpage-form-field" type="text" name="mailing_city" value=""/>
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">State</span>
				
				          <SELECT name="mailing_state" SIZE="1" class="input_200 subpage-form-field">
				            <option value="" SELECTED SELECTED>Select A State</option>
				            <option value="AA"  >APO/FPO-Americas</option><option value="AE"  >APO/FPO-Middle East</option><option value="AP"  >APO/FPO-Pacific</option><option value="AK"  >Alaska</option><option value="AL"  >Alabama</option><option value="AR"  >Arkansas</option><option value="AS"  >American Samoa</option><option value="AZ"  >Arizona</option><option value="CA"  >California</option><option value="CO"  >Colorado</option><option value="CT"  >Connecticut</option><option value="DC"  >District of Columbia</option><option value="DE"  >Delaware</option><option value="FL"  >Florida</option><option value="GA"  >Georgia</option><option value="GU"  >Guam</option><option value="HI"  >Hawaii</option><option value="IA"  >Iowa</option><option value="ID"  >Idaho</option><option value="IL"  >Illinois</option><option value="IN"  >Indiana</option><option value="KS"  >Kansas</option><option value="KY"  >Kentucky</option><option value="LA"  >Louisiana</option><option value="MA"  >Massachusetts</option><option value="MD"  >Maryland</option><option value="ME"  >Maine</option><option value="MI"  >Michigan</option><option value="MN"  >Minnesota</option><option value="MO"  >Missouri</option><option value="MS"  >Mississippi</option><option value="MT"  >Montana</option><option value="NC"  >North Carolina</option><option value="ND"  >North Dakota</option><option value="NE"  >Nebraska</option><option value="NH"  >New Hampshire</option><option value="NJ"  >New Jersey</option><option value="NM"  >New Mexico</option><option value="NV"  >Nevada</option><option value="NY"  >New York</option><option value="OH"  >Ohio</option><option value="OK"  >Oklahoma</option><option value="OR"  >Oregon</option><option value="PA"  >Pennsylvania</option><option value="PR"  >Puerto Rico</option><option value="RI"  >Rhode Island</option><option value="SC"  >South Carolina</option><option value="SD"  >South Dakota</option><option value="TN"  >Tennessee</option><option value="TX"  >Texas</option><option value="UT"  >Utah</option><option value="VA"  >Virginia</option><option value="VT"  >Vermont</option><option value="WA"  >Washington</option><option value="WI"  >Wisconsin</option><option value="WV"  >West Virginia</option><option value="WY"  >Wyoming</option></select>
						
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">ZIP</span>
						<input type="text" name="mailing_zip" value="" class="subpage-form-field"/>
					</div>
					<div class="subpage-form-item">
						<!-- <input type="checkbox" name="" value=""/> Check if mailing address differs from billing address -->
					</div>
				</div>
				
				<div class="subpage-payment">
					<h2>Payment Info</h2><img src="images/payment-cc.png" class="subpage-payment-cc-info"/>
					<div class="subpage-clear"></div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">Payment Method:</span>
						<select class="subpage-form-field" name="payment_method" id="selectbasic" required>
						  	<option value="" selected>Select a payment method</option>
			                <option value="MC">MasterCard</option>
			                <option value="VISA">Visa</option>
							<option value="AMEX">American Express</option>
					    </select>
					</div>
					<div class="subpage-form-item">
						<span class="subpage-form-label">Credit Card Number:</span>
						<input class="subpage-form-field" type="creditcard" name="payment_cc" value="" required/>
					</div>
					<div class="subpage-form-item exp-month">
						<span class="subpage-form-label">Expiration Date:</span>
						<select class="subpage-form-field exp_month" name="payment_exp_month" id="selectbasic" required>
							<option value="" selected>Month</option>
							<option value="01">01</option>
							<option value="02">02</option>
							<option value="03">03</option>
							<option value="04">04</option>
							<option value="05">05</option>
							<option value="06">06</option>
							<option value="07">07</option>
							<option value="08">08</option>
							<option value="09">09</option>
							<option value="10">10</option>
							<option value="11">11</option>
							<option value="12">12</option>
					    </select>
					</div>
					<div class="subpage-form-item exp-year">
						<select class="subpage-form-field exp_year" name="payment_exp_year" id="selectbasic" required>
							<option value="" selected>Year</option>
							<option value="13">2013</option>
							<option value="14">2014</option>
							<option value="15">2015</option>
							<option value="16">2016</option>
							<option value="17">2017</option>
							<option value="18">2018</option>
							<option value="19">2019</option>
							<option value="20">2020</option>
							<option value="21">2021</option>
							<option value="22">2022</option>
					    </select>
					</div>
					
					<div class="subscription-text">
						<strong>*Automatic Renewal Program:</strong> Your subscription will be automatically renewed unless you tell us to stop. Before the start of each renewal, you will be sent a reminder notice stating the term and rate then in effect. If you do nothing, your credit/debit card will be charged or you will be sent an invoice for your subscription. You may cancel at any time during subscription and receive a full refund for unmailed issues.
					</div>
				</div>
				
				<INPUT TYPE="HIDDEN" name="promotionkey" value="<?php print $promotionKey['special']['a']; ?>"/> 
				
				<div class="subpage-submit-form">
					<input type="submit" value="Subscribe"/>
					<p>Login to your <a href="https://w1.buysub.com/servlet/CSGateway?cds_mag_code=WSH">Subscriber Services</a> account to update your subscription settings. Or call 202-331-0715<br/>to speak with a member of our circulation team.</p>
				</div>
				
			</div>
			</form>
		</div>
		
		<div id="subpage-col-right">
			<br><br><img src="images/cover-august2013.jpg" class="magazine-cover">
		</div>
	</div>
	<div id="subpage-footer"></div>
</div>
<script>
$("#specialForm").validate();
</script>
</body>
</html>