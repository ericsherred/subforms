<?php 

		$email 								=		$_GET['email'];
		$firstname							=		$_GET['firstname'];
		$lastname							=		$_GET['lastname'];
		$address1							=		$_GET['address1'];
		$address2							=		$_GET['address2'];
		$city								=		$_GET['city'];
		$state								=		$_GET['state'];
		$zipcode							=		$_GET['zipcode'];
		$country							=		$_GET['country'];
		$value								=		$_GET['value'];
		$date							 	=		date_create();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Washingtonian Magazine - Dining and Restaurants, Shopping, Politics, Entertainment, Nightlife, Real Estate, News and Events in Washington, DC, Maryland and Virginia</title>

<link type="text/css" rel="stylesheet" href="http://www.washingtonian.com/wol.css" />
<link type="text/css" rel="stylesheet" href="http://www.washingtonian.com/include/fonts/fontello4/css/fontello.css" />
<link type="text/css" rel="stylesheet" href="http://www.washingtonian.com/include/fonts/fontello4/css/animation.css" />
<!--[if IE 7]>
<link rel="stylesheet" href="http://www.washingtonian.com/include/fonts/fontello4/css/fontello-ie7.css">
<![endif]-->


<link type="text/css" rel="stylesheet" href="http://www.washingtonian.com/navigation.css" />
<link type="text/css" rel="stylesheet" href="http://www.washingtonian.com/colorbox.css" />
<link type="text/css" rel="stylesheet" href="washingtonian_CDS.css" media="screen" />





<meta name="Keywords" content="washingtonian magazine, washingtonian magazine online, washington dc guide, virginia guide, maryland guide, washington dc recreation, virginia recreation, maryland recreation, washington dc restaurants, virginia restaurants, arlington restaurants, alexandria restaurants, maryland restaurants, washington dc events, arlington events, washington dc entertainment, virginia entertainment, maryland entertainment, washington dc attractions, virginia attractions, maryland attractions, washington dc happy hours, virginia happy hours, arlington happy hours, maryland happy hours, washington dc outdoor, virginia outdoor, maryland outdoor, washington dc dining, virginia dining, arlington dining, alexandria dining, maryland dining, washington dc bars, arlington bars, virginia bars, maryland bars, washington dc nightlife, virginia nightlife, arlington nightlife, maryland nightlife, washington dc shopping, virginia shopping, maryland shopping, washington dc fashion, washington dc visitors guide, virginia visitors guide, maryland visitors guide, arlington visitors guide, washington dc travel, virginia travel, maryland travel, washington dc top doctors, virginia top doctors, maryland top doctors, washington dc best doctors, washington dc weddings, virginia weddings, maryland weddings, washington dc blogs, washington dc spa, virginia spa, maryland spa, washington dc day spas, washington dc spas, restaurant week, washington dc restaurant week, washington dc brunch, virginia brunch, maryland brunch, washington dc pets, washington dc lawyers, best lawyers, virginia lawyers, maryland lawyers, washington dc homes, virginia homes, maryland homes, washington dc real estate, virginia real estate, maryland real estate, what to do in washington dc, northern virginia, capitol hill, dupont circle, washington dc capitol, washington dc capital, national mall, washington dc theater, washington dc neighborhoods" />


<meta name="robots" content="all" />  
<meta name="google-site-verification" content="vvVTsFEd7-c5BCuE1p4P_yz2s99Ensy3QhC-odFrCjQ" /> 
<meta property="fb:admins" content="100002475007199" />

<link rel="shortcut icon" href="http://www.washingtonian.com/images/wash.ico"/>




<script href="http://www.washingtonian.com//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

<script href="http://www.washingtonian.com/organictabs.jquery.js"></script>

<script href="http://www.washingtonian.com/slider.js"></script>


<script href="http://www.washingtonian.com/browser-selector.js"></script>
<script href="http://www.washingtonian.com/jquery.colorbox-min.js"></script>
<script href="http://www.washingtonian.com/sessvars.js"></script>


     <!-- BLOG TABS -->
	 <script>
        $(function() {
    		$("#homepage-top-recent-blog-entries").organicTabs({
                "speed": 300
            });    
        });
    </script>



<!-- CALENDAR SCRIPT - documentation here: http://arshaw.com/fullcalendar/ -->
<script type='text/javascript' src='/fullcalendar.min.js'></script>
<script type='text/javascript'>
  function zeroFill( number, width ) {
    width -= number.toString().length;
    if( width > 0 ) {
      return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
    }
    return number;
  }
  $(document).ready(function() {
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    $('#calendar').fullCalendar({
      dayClick: function(date, start, end, jsEvent, url) {
        top.location='/events/daily.php?date=' + zeroFill(date.getFullYear(),2) + zeroFill(date.getMonth()+1,2) + zeroFill(date.getDate(),2); 
      },
      editable: false
    });
    $('#wedding-calendar').fullCalendar({
      dayClick: function(date, start, end, jsEvent, url) {
        top.location='/weddings/events/daily.php?date=' + zeroFill(date.getFullYear(),2) + zeroFill(date.getMonth()+1,2) + zeroFill(date.getDate(),2); 
      },
      editable: false
    });
  });
</script>






<script type='text/javascript' src='http://partner.googleadservices.com/gampad/google_service.js'>
</script>
<script type='text/javascript'>
GS_googleAddAdSenseService("ca-pub-7659396398034687");
GS_googleEnableAllServices();
</script>

<script type='text/javascript'>
var btbucketsBTB = $BTB.getAllUserBuckets();
for (BTBx in btbucketsBTB)  
{
 GA_googleAddAttr(btbucketsBTB[BTBx].substring(0,10), "true");  
}
</script>



	
		
		<!-- here.googleAddInsertion --><script language="JavaScript"> 
		// the keywords are:  
		</script> 
		
	

<script type='text/javascript'>

GA_googleAddSlot("ca-pub-7659396398034687", "Homepage_Leaderboard_Bottom");

GA_googleAddSlot("ca-pub-7659396398034687", "Homepage_MediumRectangle_300x250");

GA_googleAddSlot("ca-pub-7659396398034687", "Homepage_Tile_Right");

GA_googleAddSlot("ca-pub-7659396398034687", "Homepage_Tile_Left");

GA_googleAddSlot("ca-pub-7659396398034687", "homepage_dropdown_890x190");

GA_googleAddSlot("ca-pub-7659396398034687", "Homepage_Bottom_Fixed_900x80");

</script>
<script type='text/javascript'>
GA_googleFetchAds();
</script>

<script language="javascript">
    function clearText(field){
        if (field.defaultValue == field.value) field.value = '';
        else if (field.value == '') field.value = field.defaultValue;
    }
</script>

    
        
        
        
        
        
        
        <link rel="alternate" type="application/atom+xml" title="Washingtonian" href="http://feeds.feedburner.com/WashingtonianBlogs" />
        <link rel="alternate" type="application/rss+xml" title="Washingtonian Blogs" href="http://www.washingtonian.com/blogs/rss.xml" />


        <script type="text/javascript" src="http://www.washingtonian.com/mt.js"></script>
    

<style>
#home-page-wrapper {
	background:url('/images/dots-inner.png') 255px 0 repeat-y;
}
</style>

<script type="text/javascript">
  //<![CDATA[[
  $SA={s:15014,asynch:1};
  (function(){
      var sa = document.createElement("script"); sa.type = "text/javascript"; sa.async = true;
      sa.src = ("https:" == document.location.protocol ? "https://" + $SA.s + ".sa" : "http://" + $SA.s + ".a") + ".siteapps.com/" + $SA.s + ".js";
      var t = document.getElementsByTagName("script")[0]; t.parentNode.insertBefore(sa, t);
  })();
  //]]>
</script>
</head>
<body>



<div id="fb-root"></div>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '244251412284157', // App ID
      channelUrl : '//www.washingtonian.com/channel.php', // Channel File
      status: true, cookie: true, xfbml: true
    });
  };

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=244251412284157";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>


<!-- BEGIN PRIMARY PAGE CONTAINER -->
<div id="page-container">
  <script type="text/javascript">
    $(document).ready(function() {
      $('#top-header #other-links li').click(function(){
        $(this).addClass('hover');
      });
        
      $('#other-links li div.courtesy-drop').mouseleave(function(){
        $(this).parent().removeClass('hover');
      });
    });
  </script>

  <!-- BEGIN PAGE HEADER CONTAINER -->
  <div id="page-header-container">
    <!-- BEGIN PAGE TOP BAR HEADER -->
    
    <div id="top-header">
      <div id="other-links">
        <ul>
          <li>
            <a href="javascript:void(0);">Magazine Login <img href="http://www.washingtonian.com/images/down-arrow-nav.png" /></a>
            <div id="mag-login" class="courtesy-drop">
              <h4>Read This Month's Issue</h4>
              <img src="http://www.washingtonian.com/articles/images/2013_Jul-small.jpg" class="cover" width="140" />
              <h5>Desktop Edition</h5>
              <p>Read a replica of this month's magazine on your computer. It's free for print subscribers.
                <a href="http://www.washingtonian.com/tablet/">Read Now</a>
              </p>
              <h5>iPad Edition</h5>
              <p>
                Read a digital version of this month's magazine on your iPad. It's free for print subscribers.
                <a href="http://www.washingtonian.com/ipad-app">Download Now</a>
              </p>
            </div>
          </li>
          <li>
            <a href="javascript:void(0);">Website Login <img href="http://www.washingtonian.com/images/down-arrow-nav.png" /></a>
            <div id="web-login" class="courtesy-drop">
              <h4>Sign in to Washingtonian.com</h4>
              <p>
                A Washingtonian.com account will allow you to manage services offered through our website.
                <span style="display:block; height:6px;"></span>
                
                
                                  
                    <a href="http://www.washingtonian.com/member/member/index/" class="memberBtn">Go to Member Login Page</a>
                    <a href="http://www.washingtonian.com/member/signup/index/c/create_account" class="memberBtn">Create a New Account</a>
                
                                  
              </p>
            </div>
          </li>
          <li><a href="https://w1.buysub.com/pubs/WH/WSH/WSH_Convertible.jsp?cds_page_id=60116">Subscribe</a></li>
          <li><a href="http://www.washingtonian.com/advertise/">Advertise</a></li>
      
          
        </ul>
      </div><!-- #other-links -->
      
      <div id="search-form">
        <form name="qs" action="/search_site/finder.php">
          <div id="search-field">
            <input class="search" id="searchField" name="search" value="Search the Washingtonian" onblur="clearText(this);" onfocus="clearText(this);" type="text">
          </div>
          <div id="search-button">
            <input name="btnG" id="searchButton" href="http://www.washingtonian.com/images/search_button_2.png" type="image">
          </div>
        </form>
      </div><!-- #search-form --> 
      
      <div id="social-media">
        <ul>
          <li><a href="http://feeds.feedburner.com/WashingtonianBlogs" target="_new" class="rss"></a></li>                  
          <li><a href="http://www.twitter.com/washingtonian" class="twitter" target="_new"></a></li>
          <li><a href="http://www.facebook.com/washingtonian" target="_new" class="facebook"></a></li>
          <li><a href="http://www.pinterest.com/washingtonians" target="_new" class="pinterest"></a></li>
        </ul> 
      </div>
      
    </div><!-- #top-header -->
    
    <!-- END PAGE TOP BAR HEADER -->  
  <!-- BEGIN LOGO HEADER -->
  <div id="logo-header" class="clearfix">
    <!-- BEGIN LEFT ADVERTISMENT -->
    <div id="left-advertisment" class="left">
       
        
        
          
  
    
    
      <!-- Homepage_Tile_Left -->
<script type='text/javascript'>
GA_googleFillSlot("Homepage_Tile_Left");
</script>

    
  

        
            
    </div>
    <!-- END LEFT ADVERTISMENT -->
    <!-- BEGIN HEADER LOGO -->
    <div id="washingtonian-logo" class="left"><a href="http://www.washingtonian.com/" target="_self"><img src="http://www.washingtonian.com/images/wash-logo.png" alt="Washingtonian" width="348" /></a></div>
    <!-- END HEADER LOGO -->
    <!-- BEGIN RIGHT SIDE ADVERTISMENT -->
    <div id="right-advertisment" class="right">
       
        
        
          
  
    
    
      <!-- Homepage_Tile_Right -->
<script type='text/javascript'>
GA_googleFillSlot("Homepage_Tile_Right");
</script>

    
  

          
      
    </div>
    <!-- END RIGHT SIDE ADVERTISMENT -->
  </div>
  <!-- END LOGO HEADER -->

  
  
    <!-- BEGIN PRIMARY NAVIGATION -->
<div id="primary-navbar">
  <div id="sections">
    <div id="nav1">
      <ul class="dropmenu clearfix" id="nav-one">
        <li><a href="http://www.washingtonian.com/sections/food-drink/" id="food">Food &amp; Drink</a>
          <div class="food clearfix">
            <ul class="left">
              <li><a href="http://www.washingtonian.com/packages/food-drink/the-100-very-best-restaurants-2013/">100 Best Restaurants</a></li>
              <li><a href="http://www.washingtonian.com/blogs/bestbites/">Best Bites Blog</a></li>
              <li><a href="http://www.washingtonian.com/packages/food-drink/cheap-eats-2012/">Cheap Eats</a></li>
              <li><a href="http://www.washingtonian.com/badges/dirt-cheap-eats-2009.php">Dirt Cheap Eats</a></li>
            </ul>
            <ul class="right">
              <li><a href="http://www.washingtonian.com/packages/food-drink/great-bars-2013/">Great Bars</a></li>
              <li><a href="http://www.washingtonian.com/happyhours/">Happy Hour Finder</a></li>
              <li><a href="http://www.washingtonian.com/chats/kliman">Kliman Online</a></li>
              <li><a href="http://www.washingtonian.com/restaurantreviews/">Restaurant Finder</a></li>
            </ul>
          </div> <!-- .food -->
        </li>
        <li><a href="http://www.washingtonian.com/sections/health/">Health</a>
          <div>
            <ul>
              <li><a href="http://www.washingtonian.com/healthcare/finder.php?type=dentist">Top Dentists</a></li>
              <li><a href="http://www.washingtonian.com/healthcare/">Top Doctors</a></li>
              <li><a href="http://www.washingtonian.com/packages/style/great-day-spas-2008/">Spas</a></li>
              <li><a href="http://www.washingtonian.com/blogs/wellbeing/">Well+Being Blog</a></li>
            </ul>
          </div>
        </li>
        <li><a href="http://www.washingtonian.com/sections/homes/">Homes</a>
          <div>
            <ul>
              <li><a href="http://www.washingtonian.com/sections/homegarden/home-design/">Home Design</a></li>
              <li><a href="http://www.washingtonian.com/homerepair/">Home &amp; Garden Database</a></li>
              <li><a href="http://www.washingtonian.com/blogs/openhouse/">Open House Blog</a></li>
              <li><a href="http://www.washingtonian.com/sections/homegarden/automotive/car-talk/">Automotive</a></li>
          </ul>
          </div>
        </li>
        <li><a href="http://www.washingtonian.com/sections/politics-personalities/">People &amp; Politics</a>
          <div>
            <ul>
              <li><a href="http://www.washingtonian.com/blogs/capitalcomment/">Capital Comment Blog</a></li>
              <li><a href="http://www.washingtonian.com/blogs/capitalcomment/sports">Sports</a></li>
              <li><a href="http://www.washingtonian.com/blogs/dead_drop/">Dead Drop Blog</a></li>
             <li><a href="http://www.washingtonian.com/washingtonvoices/">Washington Voices</a></li>
         </ul>
          </div>
        </li>
        <li><a href="http://www.washingtonian.com/sections/style/">Style</a>
          <div>
            <ul>
              <li><a href="http://www.washingtonian.com/blogs/shoparound/">Shop Around Blog</a></li>
              <li><a href="http://www.washingtonian.com/sections/style/great-day-spas-2013/">Spas</a></li>
              <li><a href="http://www.washingtonian.com/spareviews/">Find a Spa</a></li>
            </ul>
          </div>
        </li>
        <li><a href="http://www.washingtonian.com/sections/travel/">Travel</a>
          <div>
            <ul>
              <li><a href="http://www.washingtonian.com/blogs/getaways/">Great Getaways Blog</a></li>
              <li><a href="http://www.washingtonian.com/hotelreviews/">Hotel Finder</a></li>
              <li><a href="http://www.washingtonian.com/packages/travel/weekend-getaways-2011/index.php">Weekend Trips</a></li>
            </ul>
          </div>
        </li>
        <li><a href="http://www.washingtonian.com/sections/where-when/">Where &amp; When</a>
          <div>
            <ul>
              <li><a href="http://www.washingtonian.com/blogs/afterhours/">After Hours Blog</a></li>
              <li><a href="http://www.washingtonian.com/events/">Events Calendar</a></li>
              <li><a href="http://www.washingtonian.com/happyhours/">Happy Hour Finder</a></li>
              <li><a href="http://www.washingtonian.com/packages/food-drink/great-bars-2013/">Great Bars</a></li>
            </ul>
          </div>
        </li>
        <li><a href="http://www.washingtonian.com/weddings/">Weddings</a>
          <div>
            <ul>
              <li><a href="http://www.washingtonian.com/blogs/bridalparty/">Bride &amp; Groom Blog</a></li>
              <li><a href="http://www.washingtonian.com/weddings/vendors/">Wedding Vendors</a></li>
            </ul>
          </div>
        </li>
        <li><a href="http://www.washingtonian.com/sections/work-family/" class="lastlink">Work &amp; Family</a>
          <div class="lastnav">
            <ul>
              <li><a href="http://www.washingtonian.com/petvendors/">Pets</a></li>
              <li><a href="http://www.washingtonian.com/packages/work-family/2010-guide-to-private-schools/index.php">Private School Guides</a></li>
              <li><a href="http://www.washingtonian.com/packages/work-family/great-places-to-work-2011/">Great Places to Work</a></li>
              <li><a href="http://www.washingtonian.com/blogs/harry/">Ask Harry &amp; Louise</a></li>           
           </ul>
          </div>
        </li>
      </ul>
    </div> <!-- #nav1 -->
  </div>
</div>
<!-- END PRIMARY NAVBAR -->
<!-- BEGIN SUB NAVBAR -->
<div id="sub-navbar">
  <div id="nav2">
    <ul class="dropmenu2 clearfix">
      <li><a href="http://www.washingtonian.com/sections/real-estate/" id="photos-nav">Real Estate</a></li>
      <li><a href="http://www.washingtonian.com/sections/neighborhood-guides/index.php">Neighborhood Guides</a></li>
      <li><a href="http://www.washingtonian.com/sections/visitors-guide/">Visitors Guide</a></li>
      <li><a href="http://www.washingtonian.com/packages/best-of-washington-party/best-of-washington-2013-party/">Best of Washington</a></li>
      <li><a href="http://www.washingtonian.com/healthcare/">Top Doctors</a></li>
      <li><a href="http://www.washingtonian.com/packages/politics-personalities/top-lawyers-2011-2012/index.php">Top Lawyers</a></li>
      <li><a href="http://www.washingtonian.com/happyhours/">Bars</a></li>
      <li><a href="http://www.washingtonian.com/restaurantreviews/reviews.php">Restaurants</a></li>
      <li><a href="http://www.washingtonian.com/events/" class="lastlink">Events Calendar</a></li>
    </ul>
  </div>
</div>
<!-- END SUB NAVBAR -->
</div>
<!-- END PAGE HEADER CONTAINER -->            
  <div id="main-content-area" class="clearfix">  
  
  <div style="clear:both;"></div>
  
  <div id="single-column-article" class="clearfix">
        
        
            
            
    <div id="page-sidebar-container" class="clearfix">
    	
	<!-- BEGIN PAGE SIDEBAR POSITION 1 -->
        <div id="sidebar-item-top">
        	
            <!-- BEGIN USER TOOLS -->
            <div id="user-tools" class="clearfix" style="text-align:center; border:1px solid #888;">
        <h4>Manage Your Account</h4>
        	<a href="https://w1.buysub.com/servlet/GiftsGateway?cds_mag_code=WSH&cds_page_id=60351" style="margin:6px auto; font-weight:200;">Give a Gift</a>
            <a href="https://w1.buysub.com/servlet/CSGateway?cds_mag_code=WSH" style="margin:6px auto; font-weight:200;">Subscriber Services</a>
            <a href="/pubs/WH/WSH/WSH_FAQs.jsp?lsid=21221317436015952&vid=1&cds_page_id=60362&cds_mag_code=WSH" style="margin:6px auto; font-weight:200;">Frequently Asked Questions</a>
		</div>            
            <!-- END USER TOOLS -->
        </div>
        <!-- END PAGE SIDEBAR POSITION 1 -->
        
        <!-- BEGIN SIDEBAR POSITION 2 -->
        <div id="sidebar-item" class="with-border">
        <div id="iPad-promo">
        <a href="http://www.washingtonian.com/ipad" target="_blank"><img src="images/ipad-inhouse.png" border="0" /></a>
        <p style="text-align:center; position:relative; top:-8px; color:#444;">Print subscribers get free access to our iPad edition. <a href="http://www.washingtonian.com/ipad" target="_blank" style="margin-bottom:15px;">Download Now!</a></p>
        </div>        
        </div>
        <!-- END SIDEBAR POSITION 2 -->
        
         <!-- BEGIN SIDEBAR POSITION 3 -->
        <div id="sidebar-item" class="with-border">
        <div id="iPad-promo">
        <a href="http://mydigimag.rrd.com/publication?m=18795&l=1" target="_blank"><img src="images/digital-edition.png" border="0" style="display:block; margin-bottom:12px;" /></a>
        <p style="text-align:center; position:relative; top:-8px; color:#444;">Print <strong>AND</strong> iPad subscribers get free access to our digital edition. <a href="http://mydigimag.rrd.com/publication?m=18795&l=1" target="_blank">Try It Now!</a></p>
        </div>        
        </div>
        <!-- END SIDEBAR POSITION 3 -->
      
   </div> <!-- #page-sidebar-container -->
  
   <!-- BEGIN PAGE CONTENT CONTAINER -->
   <div id="page-main-content-container" class="right clearfix article-display-case"> 
	
    <!-- BEGIN SECTION ARTICLES -->
        <div id="section-article">   
        
  
<div class="CDS_main_area">
    	<div id="mag_header_area">
        <img src="graphics/2012_May-2.jpg" alt="latest cover" width="95" align="left" />
           <div id="callout"><h1>Thank you from Washingtonian Magazine</h1>
            <em>Your source for dining, nightlife, news, shopping and more in the Washington region.</em>
            </div>
            <div class="clear"></div>

        </div>
        <div id="gift_recipients"><!--Begin Gift Recipients-->
          <table border="0" cellpadding="0" cellspacing="0" class="whiteborder">
        <tr>
          
          
        </tr>
      </table>
    
    </div>
        
        <form name="convertible_form" action="../order-processing-curl.php" method="post">
<!-- RESPONSE KEY:  I12SR -->
<INPUT TYPE="HIDDEN" NAME="lsid" VALUE="21221317436015952">
<INPUT TYPE="HIDDEN" NAME="vid" VALUE="1">
<INPUT TYPE="HIDDEN" NAME="cds_page_id" VALUE="60116">
<INPUT TYPE="HIDDEN" NAME="cds_mag_code" VALUE="WSH">
<INPUT TYPE="HIDDEN" NAME="cds_form_submitted" VALUE="WSH_Convertible:convertible_form">
<INPUT TYPE="HIDDEN" NAME="cds_doms_order_number" VALUE="">
        <div style="color:#F00;">
          
        </div>
        <div class="box_bkg">
       
       <!-- BEGIN SUBSCRIPTION TYPE INFORMATION -->
        <div id="Payment_info"><!--Begin Subscription Info-->
        
        

        <div class="CF_headline" style="margin-top:10px;">Subscription Confirmation</div>
         
        <div class="confirmation-text">
	        
	        <p>Thank you for becoming a digital subscriber to Washingtonian.</p>
	        <p>To log into your iPad edition, please <a href="#">click here</a>.</p>
	        <p>	User Name: <?php print $email;?> <br/>
	        	Password: <?php print $zipcode;?> 
	        </p>
	        <p>If you have any questions, do not hesitate to contact us.</p>
	        <p>	202-331-0715 M-F 9a-5p EST <br/>
	        	800-365-7050 M-F 8a-10:30p, Sat 9a-7p EST <br/>
	        	<a href="mailto:washsub@washingtonian.com">washsub@washingtonian.com</a>
	        </p>
	        <p>
	        
	        <div class="CF_headline" style="margin-top:10px;">Billing Information</div>
	        
	        <p>
	        	<strong>Name:</strong> <?php print $firstname;?> <?php print $lastname;?><br/>
	        	<strong>Address:</strong> <?php print $address1 . "<br/>" . $address2;?> <br/>
	        	<strong>City:</strong> <?php print $city;?> <br/>
	        	<strong>State:</strong> <?php print $state;?> <br/>
	        	<strong>Zip/Postal Code:</strong> <?php print $zipcode;?> <br/>
	        	<strong>Email Address:</strong> <?php print $email;?> <br/>
	        </p>
	        
        </div>
        
        
      </div><!-- end Subscription Type -->
      
      
        
        
        <div id="Payment_info"><!--Begin Payment Info-->
          <div id="TV">


	 </div>
        
            
          <!--End Gift Checkbox-->
          </div><!--End Payment Info-->
          
          
          <div class="clear"></div>
        </div>
        
        <!-- DONEE_INFO DELETED WITHIN THIS SECTION -->

    
    </form>
    
  </div>
</div>

</div> <!-- section article -->
</div> <!-- left column -->
</div> <!-- end website main content area -->

<div style="clear:both; width:100%;"></div>
  
    
    
<!-- Footer -->
<div id="page-footer-container">
<!-- BEGIN FOOTER LINKS -->
    <div id="footer-links" class="clearfix">

            <div id="links-logo"> <a href="http://www.washingtonian.com" target="_self"><img src="images/wash-logo-footer.png" alt="washingtonian logo" /></a>
            </div>
            <div id="links">
            <a href="http://www.washingtonian.com" target="_self">Home</a>
</div>
            <div id="links">
            <a href="https://w1.buysub.com/servlet/ConvertibleGateway?cds_mag_code=WSH&cds_page_id=60116" target="_new">Subscribe</a>
</div>
            <div id="links">
			<a href="http://www.washingtonian.com/advertise/index.php" target="_self">Advertise</a>
			</div>
            
            <div id="links">
			<a href="http://www.washingtonian.com/sections/special-events/index.php" target="_self">Special Events</a>
			</div>
            
            <div id="links">
			<a href="http://www.washingtonian.com/covers/2012-covers/index.php" target="_self">Cover Archive</a>
			</div>
            
            <div id="links">
            <a href="http://www.washingtonian.com/internships-at-the-washingtonian/index.php">Internships</a>
            </div>
            <div id="links">
            <a href="http://www.washingtonian.com/about-us/index.php">About Us</a>
            </div>

            <div id="links">
            <a href="http://www.washingtonian.com/welcome-to-the-washingtonian/index.php">Letter from the Editor</a>
            </div>
            <div id="links">
            <a href="http://www.washingtonian.com/writers-guidelines/index.php">Writer's Guidelines</a>
            </div>
            <div id="links">
            <a href="http://www.washingtonian.com/newsletters/index.php">Email Newsletters</a>

            </div>
      </div>
        <!-- END FOOTER LINKS -->
        
        <!-- BEGIN FOOTER SUB LINKS -->
<div id="footer-sub-links">
            <div id="links">
                <a href="http://www.washingtonian.com/contact-us/index.php">Contact Us</a> 
                <a href="http://www.washingtonian.com/privacy-policy/index.php">Privacy Policy</a> 
                <a href="http://www.washingtonian.com/site-map/index.php">Site Map</a>
                &copy;2012 Washington Magazine, Inc.
                </div>
                
            </div>
        <!-- END FOOTER SUB LINKS -->


</div>
<!-- End Footer -->

</div>
<!-- END PRIMARY PAGE CONTAINER -->

<!-- Google Code for Subscription Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1068338664;
var google_conversion_language = "en";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "R7OBCNDB4QkQ6Ju2_QM";
var google_conversion_value = <?php print $value;?>;
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1068338664/?value=<?php print $value;?>&amp;label=R7OBCNDB4QkQ6Ju2_QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- OMNITURE READY -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1860879-1']);
  _gaq.push(['_setDomainName', 'washingtonian.com']);
  _gaq.push(['_trackPageview']);
  _gaq.push(['_addTrans',
    '<?php print date_timestamp_get($date); ?>',    	    // transaction ID - required
    'Washingtonian', 										// affiliation or store name
    '<?php print $value;?>',      							// total - required
    '0',         											// tax
    '0',              										// shipping
    '<?php print $city;?>',      							// city
    '<?php print $state;?>',     							// state or province
    'USA'             										// country
  ]);
  _gaq.push(['_addItem',
    '<?php print date_timestamp_get($date); ?>',     		// transaction ID - required
    '001',          										// SKU/code - required
    'Subscription',        									// product name
    '<?php print $firstname;?> <?php print $lastname;?>',   // category or variation
    '<?php print $value;?>',        		  				// unit price - required
    '1'               										// quantity - required
  ]);  
  _gaq.push(['_trackTrans']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

//Bad Robot Detection

	var category = 'trafficQuality';
	var dimension = 'botDetection';
	var human_events = ['onkeydown','onmousemove'];

	if ( navigator.appName == 'Microsoft Internet Explorer' && !document.referrer) {
		for(var i = 0; i < human_events.length; i++){
			document.attachEvent(human_events[i], ourEventPushOnce);
		}
	}else{
		_gaq.push( [ '_trackEvent', category, dimension, 'botExcluded', 1, true ] );
	}

	function ourEventPushOnce(ev) {

		_gaq.push( [ '_trackEvent', category, dimension, 'on' + ev.type, 1, true ] );

		for(var i = 0; i < human_events.length; i++){
			document.detachEvent(human_events[i], ourEventPushOnce);
		}

	} // end ourEventPushOnce()

	//End Bad Robot Detection

</script>

</body>
</html>

