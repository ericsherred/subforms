<?php 
/* 	
	CONTROL FORM B (standard, default form -- WITH VARIATION: offer prices are -$5.00 with verbiage that includes $5 shipping). 
	MS Note: The "control" forms contain remnant HTML code from the original CDS-hosted form, some of which contain hidden form values that are not being used by the custom self-hosted app. 
	See "subscribe/special" forms for complete custom/from-scratch coding necessary for the self-hosted app to function properly. 
*/

require('../_includes/scripts.inc');
include('header_current.html');
?>
  <div id="main-content-area" class="clearfix">  
  
  <div style="clear:both;"></div>
  
  <div id="single-column-article" class="clearfix">
            
    <div id="page-sidebar-container" class="clearfix">
    	
	<!-- BEGIN PAGE SIDEBAR POSITION 1 -->
        <div id="sidebar-item-top">
        	
            <!-- BEGIN USER TOOLS -->
            <div id="user-tools" class="clearfix" style="text-align:center; border:1px solid #888;">
        <h4>Manage Your Account</h4>
        	<a href="https://w1.buysub.com/servlet/GiftsGateway?cds_mag_code=WSH&cds_page_id=60351" style="margin:6px auto; font-weight:200;">Give a Gift</a>
            <a href="https://w1.buysub.com/servlet/CSGateway?cds_mag_code=WSH" style="margin:6px auto; font-weight:200;">Subscriber Services</a>
            <a href="/pubs/WH/WSH/WSH_FAQs.jsp?lsid=21221317436015952&vid=1&cds_page_id=60362&cds_mag_code=WSH" style="margin:6px auto; font-weight:200;">Frequently Asked Questions</a>
		</div>            
            <!-- END USER TOOLS -->
        </div>
        <!-- END PAGE SIDEBAR POSITION 1 -->
        
        <!-- BEGIN SIDEBAR POSITION 2 -->
        <div id="sidebar-item" class="with-border">
        <div id="iPad-promo">
        <a href="http://www.washingtonian.com/ipad-app" target="_blank"><img src="images/ipad-inhouse.png" border="0" /></a>
        <p style="text-align:center; position:relative; top:-8px; color:#444;">Print subscribers get free access to our iPad edition. <a href="http://www.washingtonian.com/ipad" target="_blank" style="margin-bottom:15px;">Download Now!</a></p>
        </div>        
        </div>
        <!-- END SIDEBAR POSITION 2 -->
        
         <!-- BEGIN SIDEBAR POSITION 3 -->
        <div id="sidebar-item" class="with-border">
        <div id="iPad-promo">
        <a href="http://mydigimag.rrd.com/publication?m=18795&l=1" target="_blank"><img src="images/digital-edition.png" border="0" style="display:block; margin-bottom:12px;" /></a>
        <p style="text-align:center; position:relative; top:-8px; color:#444;">Print <strong>AND</strong> iPad subscribers get free access to our digital edition. <a href="http://mydigimag.rrd.com/publication?m=18795&l=1" target="_blank">Try It Now!</a></p>
        </div>        
        </div>
        <!-- END SIDEBAR POSITION 3 -->
      
   </div> <!-- #page-sidebar-container -->
  
   <!-- BEGIN PAGE CONTENT CONTAINER -->
   <div id="page-main-content-container" class="right clearfix article-display-case"> 
	
    <!-- BEGIN SECTION ARTICLES -->
        <div id="section-article">   
        
  
<div class="CDS_main_area">
    	<div id="mag_header_area">
        <img src="graphics/2012_May-2.jpg" alt="latest cover" width="95" align="left" />
           <div id="callout"><h1>Subscribe to Washingtonian</h1>
            <em>Your source for dining, nightlife, news, shopping and more in the Washington region.</em>
            </div>
            <div class="clear"></div>

        </div>
        <div id="gift_recipients"><!--Begin Gift Recipients-->
          <table border="0" cellpadding="0" cellspacing="0" class="whiteborder">
        <tr>
          
          
        </tr>
      </table>
    
    </div>
        
        <form name="convertible_form" action="../_form_processing.php" id="controlForm" method="post">
<!-- RESPONSE KEY:  I12SR -->
<INPUT TYPE="HIDDEN" NAME="lsid" VALUE="21221317436015952">
<INPUT TYPE="HIDDEN" NAME="vid" VALUE="1">
<INPUT TYPE="HIDDEN" NAME="cds_page_id" VALUE="60116">
<INPUT TYPE="HIDDEN" NAME="cds_mag_code" VALUE="WSH">
<INPUT TYPE="HIDDEN" NAME="cds_form_submitted" VALUE="WSH_Convertible:convertible_form">
<INPUT TYPE="HIDDEN" NAME="cds_doms_order_number" VALUE="">

<INPUT TYPE="HIDDEN" name="promotionkey" value="<?php print $promotionKey['control']['b']; ?>"/> 

        <div style="color:#F00;">
          
        </div>
        <div class="box_bkg">
       
       <!-- BEGIN SUBSCRIPTION TYPE INFORMATION -->
        <div id="Payment_info"><!--Begin Subscription Info-->
        <div class="CF_headline" style="margin-top:10px;">Select Your Subscription</div>
         
         <div id="TV">
         <div id="spacer">
         <strong>Print + iPad + Digital</strong>
         <input name="subscribe_offer" value="1" type="radio" onclick="showRenewBtn()" required><label>DC, MD, & VA: 1 year (12 issues) for $24.95</label>
      	<br />
        <input name="subscribe_offer" value="2" type="radio" onclick="showRenewBtn()"><label>DC, MD, & VA: 2 years (24 issues) for $44.95</label>
         <br />
        
       <input name="subscribe_offer" value="3" type="radio" onclick="showRenewBtn()"><label>Outside DC, MD, & VA: 1 year (12 issues) for $34.95</label>
      <br />
      <input name="subscribe_offer" value="4" type="radio" onclick="showRenewBtn()"><label>Outside DC, MD, & VA: 2 years (24 issues) for $64.95</label><br />     
      
      </div>
      
      <div id="spacer">
      <strong>iPad + Digital</strong>
      <input name="subscribe_offer" value="5" type="radio" onclick="showRenewBtn()"><label>1 year for $14.99</label><br />
      </div>
      	<em style="font-size:12px;">Note: $5 for shipping + handling will be added to your subscription cost.</em>
      
      </div> <!-- close TV -->
      </div><!-- end Subscription Type -->
      
      
       <div id="Subscriber_info"><!--Begin Subscriber Info-->
        	<div class="CF_headline">Billing Information</div>

        	<div class="clearfix">
	        	<label>First Name:</label>
	        	<input name="firstname" class="input_200" id="cds_name" value="" maxlength="27" type="text" required />
	        </div>
	   
        	<div class="clearfix">
	        	<label>Last Name:</label>
	        	<input name="lastname" class="input_200" id="cds_name" value="" maxlength="27" type="text" required />
	        </div>
         
         <div class="clearfix">         
          <label>Address:</label>
          <input name="address1" class="input_200" id="cds_address_1" value="" maxlength="27" type="text" required />
        </div>
        
           <div class="clearfix"> 
         <label></label>
          <input name="address2" class="input_200" id="cds_address_2" value="" maxlength="27" />
          </div>
          
          <div class="clearfix">
          <label>City:</label>
          <input name="city" class="input_200" id="cds_city" value="" maxlength="13" type="text" required />
          </div>

			<div class="clearfix">
          <label>State:</label>
          <SELECT name="state" SIZE="1" class="input_200" required>
            <option value="" SELECTED SELECTED>Select A State</option>
            <option value="AA"  >APO/FPO-Americas</option><option value="AE"  >APO/FPO-Middle East</option><option value="AP"  >APO/FPO-Pacific</option><option value="AK"  >Alaska</option><option value="AL"  >Alabama</option><option value="AR"  >Arkansas</option><option value="AS"  >American Samoa</option><option value="AZ"  >Arizona</option><option value="CA"  >California</option><option value="CO"  >Colorado</option><option value="CT"  >Connecticut</option><option value="DC"  >District of Columbia</option><option value="DE"  >Delaware</option><option value="FL"  >Florida</option><option value="GA"  >Georgia</option><option value="GU"  >Guam</option><option value="HI"  >Hawaii</option><option value="IA"  >Iowa</option><option value="ID"  >Idaho</option><option value="IL"  >Illinois</option><option value="IN"  >Indiana</option><option value="KS"  >Kansas</option><option value="KY"  >Kentucky</option><option value="LA"  >Louisiana</option><option value="MA"  >Massachusetts</option><option value="MD"  >Maryland</option><option value="ME"  >Maine</option><option value="MI"  >Michigan</option><option value="MN"  >Minnesota</option><option value="MO"  >Missouri</option><option value="MS"  >Mississippi</option><option value="MT"  >Montana</option><option value="NC"  >North Carolina</option><option value="ND"  >North Dakota</option><option value="NE"  >Nebraska</option><option value="NH"  >New Hampshire</option><option value="NJ"  >New Jersey</option><option value="NM"  >New Mexico</option><option value="NV"  >Nevada</option><option value="NY"  >New York</option><option value="OH"  >Ohio</option><option value="OK"  >Oklahoma</option><option value="OR"  >Oregon</option><option value="PA"  >Pennsylvania</option><option value="PR"  >Puerto Rico</option><option value="RI"  >Rhode Island</option><option value="SC"  >South Carolina</option><option value="SD"  >South Dakota</option><option value="TN"  >Tennessee</option><option value="TX"  >Texas</option><option value="UT"  >Utah</option><option value="VA"  >Virginia</option><option value="VT"  >Vermont</option><option value="WA"  >Washington</option><option value="WI"  >Wisconsin</option><option value="WV"  >West Virginia</option><option value="WY"  >Wyoming</option>

          </select>
         </div>
         
         <div class="clearfix">        
          <label>ZIP:</label>
          <input name="zip" type="text" class="input_200" id="cds_zip" VALUE="" maxlength="6" />
         </div>
         
         <div class="clearfix">               
          <label>Email:</label>
		  <input name="email" class="input_200" id="cds_email" value="" maxlength="50" type="email" required/>
        </div>
     
        <input type="HIDDEN" name="cds_country" value="United States" />
        </div><!--End Subscriber Info-->
        
		<input type="checkbox" id="hasDiffMailing" name="mailing_diff_recipient"> Check if mailing address differs from billing address

 <div id="Subscriber_info"><!--MAILING ADDRESS INFORMATION-->
 		<div class="subpage-billing-mailing">
        	<div class="CF_headline">Mailing Address</div>

        	<div class="clearfix">
	        	<label>Full Name:</label>
	        	<input name="mailing_firstname" class="input_200" id="cds_name" value="" maxlength="27" type="text" required />
	        </div>
	   
         
         <div class="clearfix">         
          <label>Address:</label>
          <input name="mailing_address1" class="input_200" id="cds_address_1" value="" maxlength="27" type="text" required />
        </div>
        
           <div class="clearfix"> 
         <label></label>
          <input name="mailing_address2" class="input_200" id="cds_address_2" value="" maxlength="27" />
          </div>
          
          <div class="clearfix">
          <label>City:</label>
          <input name="mailing_city" class="input_200" id="cds_city" value="" maxlength="13" type="text" required />
          </div>

			<div class="clearfix">
          <label>State:</label>
          <SELECT name="mailing_state" SIZE="1" class="input_200" required>
            <option value="" SELECTED SELECTED>Select A State</option>
            <option value="AA"  >APO/FPO-Americas</option><option value="AE"  >APO/FPO-Middle East</option><option value="AP"  >APO/FPO-Pacific</option><option value="AK"  >Alaska</option><option value="AL"  >Alabama</option><option value="AR"  >Arkansas</option><option value="AS"  >American Samoa</option><option value="AZ"  >Arizona</option><option value="CA"  >California</option><option value="CO"  >Colorado</option><option value="CT"  >Connecticut</option><option value="DC"  >District of Columbia</option><option value="DE"  >Delaware</option><option value="FL"  >Florida</option><option value="GA"  >Georgia</option><option value="GU"  >Guam</option><option value="HI"  >Hawaii</option><option value="IA"  >Iowa</option><option value="ID"  >Idaho</option><option value="IL"  >Illinois</option><option value="IN"  >Indiana</option><option value="KS"  >Kansas</option><option value="KY"  >Kentucky</option><option value="LA"  >Louisiana</option><option value="MA"  >Massachusetts</option><option value="MD"  >Maryland</option><option value="ME"  >Maine</option><option value="MI"  >Michigan</option><option value="MN"  >Minnesota</option><option value="MO"  >Missouri</option><option value="MS"  >Mississippi</option><option value="MT"  >Montana</option><option value="NC"  >North Carolina</option><option value="ND"  >North Dakota</option><option value="NE"  >Nebraska</option><option value="NH"  >New Hampshire</option><option value="NJ"  >New Jersey</option><option value="NM"  >New Mexico</option><option value="NV"  >Nevada</option><option value="NY"  >New York</option><option value="OH"  >Ohio</option><option value="OK"  >Oklahoma</option><option value="OR"  >Oregon</option><option value="PA"  >Pennsylvania</option><option value="PR"  >Puerto Rico</option><option value="RI"  >Rhode Island</option><option value="SC"  >South Carolina</option><option value="SD"  >South Dakota</option><option value="TN"  >Tennessee</option><option value="TX"  >Texas</option><option value="UT"  >Utah</option><option value="VA"  >Virginia</option><option value="VT"  >Vermont</option><option value="WA"  >Washington</option><option value="WI"  >Wisconsin</option><option value="WV"  >West Virginia</option><option value="WY"  >Wyoming</option>

          </select>
         </div>
         
         <div class="clearfix">        
          <label>ZIP:</label>
          <input name="mailing_zip" type="text" class="input_200" id="cds_zip" VALUE="" maxlength="6" />
         </div>
        
     
        <input type="HIDDEN" name="cds_country" value="United States" />
        </div><!--End Subscriber Info-->

       </div><!-- END MAILING ADDRESS INFORMATION -->
        
        <div id="Payment_info"><!--Begin Payment Info-->
        <div class="CF_headline">Payment Information</div>
          <div id="TV">


	 </div>
         
          <div id="CF_Billing_info">
          
          <div class="clearfix">
            <div id="payment_dropdown">
           <label>Payment Method:</label>
         
            <select class="input_200" id="cc_cards" name="payment_method" required>
                <option value="" selected="selected">Select a payment method</option>

                <option value="MC">MasterCard</option>
                <option value="VISA">Visa</option>
				<option value="AMEX">American Express</option>
              </select>
            </div>
            <div id ="cc_radio" style="display:none;">
                       	<input name="cds_pay_type" type="radio" value="1"   id="master1" />

                        <input name="cds_pay_type" type="radio" value="2"   id="visa1" />
                    	<input name="cds_pay_type" type="radio" value="3"   id="amex1" />
            			<input name="cds_pay_type" type="radio" value="5"   id="disc1" />
                        <input name="cds_pay_type" type="radio" value="10"   id="master2" />
                        <input name="cds_pay_type" type="radio" value="11"   id="visa2" />
                    	<input name="cds_pay_type" type="radio" value="12"   id="amex2" />
            			<input name="cds_pay_type" type="radio" value="14"   id="disc2" />
              <input type="radio" name="cds_pay_type" value="6"   id="bill" />
                       </div>
                       </div>
		    
            <div class="clearfix">
            <label>Credit Card Number:</label>
           
            <input name="payment_cc" class="input_200" id="cds_cc_number" autocomplete="off" value="" maxlength="16" type="creditcard" required/>
        </div>
        
           <div class="clearfix"> 
            <label>Expiration Date:</label>
            <select name="payment_exp_month" class="input_98" id="cds_cc_exp_month" 
            style="width:80px; float:left; margin-left:25px;" required>
              <option value="" SELECTED SELECTED>Month</option>

              <option value="01"    >01</option>
              <option value="02"    >02</option>
              <option value="03"    >03</option>
              <option value="04"    >04</option>
              <option value="05"    >05</option>
              <option value="06"    >06</option>

              <option value="07"    >07</option>
              <option value="08"    >08</option>
              <option value="09"    >09</option>
              <option value="10"    >10</option>
              <option value="11"    >11</option>
              <option value="12"    >12</option>
		</select>
            <select name="payment_exp_year" SIZE="1" class="input_98" style="width:80px; float:left; margin-left:10px;" required>
              <option value="" SELECTED SELECTED>Year</option>
              <option value="12"  >2012</option><option value="13"  >2013</option><option value="14"  >2014</option><option value="15"  >2015</option><option value="16"  >2016</option><option value="17"  >2017</option><option value="18"  >2018</option><option value="19"  >2019</option><option value="20"  >2020</option><option value="21"  >2021</option><option value="22"  >2022</option>

            </select>
           </div>
           </div>
           
           <div class="clearfix" id="auto-renew-box">
				  <span style="font-size:10px;">*Automatic Renewal Program: Your subscription will be automatically renewed unless you tell us to stop. Before the start of each renewal, you will be sent a reminder notice stating the term and rate then in effect. If you do nothing, your credit/debit card will be charged or you will be sent an invoice for your subscription. You may cancel at any time during subscription and receive a full refund for unmailed issues. 
            </div>
            
          <!--End Gift Checkbox-->
          </div><!--End Payment Info-->
          
          
          <div class="clear"></div>
        </div>
        
        <!-- DONEE_INFO DELETED WITHIN THIS SECTION -->


        <div id="Submit_info" class="box_bkg_conf"><!--Begin Subscribe/Reset Section-->
          <div id="submitbtn"></div>
          
      <input name="send" type="submit" value="Subscribe" id="Submit" />
          <!--&nbsp;
          <input name="Reset" type="reset" value="Reset" />-->
        </div><!--End Subscribe/Reset Section-->
    
    </form>
    
  </div>
</div>

</div> <!-- section article -->
</div> <!-- left column -->
</div> <!-- end website main content area -->

<div style="clear:both; width:100%;"></div>
  
    
    
<!-- Footer -->
<div id="page-footer-container">
<!-- BEGIN FOOTER LINKS -->
    <div id="footer-links" class="clearfix">

            <div id="links-logo"> <a href="http://www.washingtonian.com" target="_self"><img src="images/wash-logo-footer.png" alt="washingtonian logo" /></a>
            </div>
            <div id="links">
            <a href="http://www.washingtonian.com" target="_self">Home</a>
</div>
            <div id="links">
            <a href="https://w1.buysub.com/servlet/ConvertibleGateway?cds_mag_code=WSH&cds_page_id=60116" target="_new">Subscribe</a>
</div>
            <div id="links">
			<a href="http://www.washingtonian.com/advertise/index.php" target="_self">Advertise</a>
			</div>
            
            <div id="links">
			<a href="http://www.washingtonian.com/sections/special-events/index.php" target="_self">Special Events</a>
			</div>
            
            <div id="links">
			<a href="http://www.washingtonian.com/covers/2012-covers/index.php" target="_self">Cover Archive</a>
			</div>
            
            <div id="links">
            <a href="http://www.washingtonian.com/internships-at-the-washingtonian/index.php">Internships</a>
            </div>
            <div id="links">
            <a href="http://www.washingtonian.com/about-us/index.php">About Us</a>
            </div>

            <div id="links">
            <a href="http://www.washingtonian.com/welcome-to-the-washingtonian/index.php">Letter from the Editor</a>
            </div>
            <div id="links">
            <a href="http://www.washingtonian.com/writers-guidelines/index.php">Writer's Guidelines</a>
            </div>
            <div id="links">
            <a href="http://www.washingtonian.com/newsletters/index.php">Email Newsletters</a>

            </div>
      </div>
        <!-- END FOOTER LINKS -->
        
        <!-- BEGIN FOOTER SUB LINKS -->
<div id="footer-sub-links">
            <div id="links">
                <a href="http://www.washingtonian.com/contact-us/index.php">Contact Us</a> 
                <a href="http://www.washingtonian.com/privacy-policy/index.php">Privacy Policy</a> 
                <a href="http://www.washingtonian.com/site-map/index.php">Site Map</a>
                &copy;2012 Washington Magazine, Inc.
                </div>
                
            </div>
        <!-- END FOOTER SUB LINKS -->


</div>
<!-- End Footer -->

</div>
<!-- END PRIMARY PAGE CONTAINER -->

<script>
$("#controlForm").validate();
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1860879-1']);
  _gaq.push(['_setDomainName', 'washingtonian.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

//Bad Robot Detection

	var category = 'trafficQuality';
	var dimension = 'botDetection';
	var human_events = ['onkeydown','onmousemove'];

	if ( navigator.appName == 'Microsoft Internet Explorer' && !document.referrer) {
		for(var i = 0; i < human_events.length; i++){
			document.attachEvent(human_events[i], ourEventPushOnce);
		}
	}else{
		_gaq.push( [ '_trackEvent', category, dimension, 'botExcluded', 1, true ] );
	}

	function ourEventPushOnce(ev) {

		_gaq.push( [ '_trackEvent', category, dimension, 'on' + ev.type, 1, true ] );

		for(var i = 0; i < human_events.length; i++){
			document.detachEvent(human_events[i], ourEventPushOnce);
		}

	} // end ourEventPushOnce()

	//End Bad Robot Detection

</script>

</body>
</html>

