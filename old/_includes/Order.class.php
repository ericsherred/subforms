<?php 
class Order 
{
	/* API-Configured References */
	/* MS Note: These are provided by CDS and generally should not change unless we initiate the request for CDS to change these values */
	/* API values are not secure-sensitive */
	
	//define(FORM_POSTURL, 'https://service.mycdsglobal.com/ws/service/order/WSH'); 
	//define(FORM_APPID, 'com.washingtonian.washingtonianmagazine.web'); 
	//define(FORM_PWD, '3fe7c7664c8be3c9'); 
	//Sample promotional key from Sara: I13GR 

	public function __construct () 
	{
		$form_posturl		 				=		'https://service.mycdsglobal.com/ws/service/order/WSH';
		$form_appid							=		'com.washingtonian.washingtonianmagazine.web';
		$form_pwd							=		'3fe7c7664c8be3c9';
		$xml_init_URL						= 		$form_posturl.'?appId='.$form_appid.'&pwd='.$form_pwd;
		$xml_init_URL						=		'https://service.mycdsglobal.com/ws/service/order/WSH?appId=com.washingtonian.washingtonianmagazine.web&pwd=3fe7c7664c8be3c9';
	}
	
	public function getOffer($subscribe_offer) 
	{
		switch($subscribe_offer) {
			case 1:
				$offer['price']				=		29.95;
				$offer['term']				=		12;
				$offer['description']		=		"DC, MD, & VA: 1 Year (12 issues) for $29.95";
				$offer['isLocal']			=		true;
				break;
			case 2:
				$offer['price']				=		49.95;
				$offer['term']				=		24;	
				$offer['description']		=		"DC, MD, & VA: 2 Years (24 issues) for $49.95";	
				$offer['isLocal']			=		true;	
				break;
			case 3:
				$offer['price']				=		39.95;
				$offer['term']				=		12;
				$offer['description']		=		"Outside DC, MD, & VA: 1 Year (12 issues) for $39.95";
				$offer['isLocal']			=		false;
				break;
			case 4:
				$offer['price']				=		69.95;
				$offer['term']				=		24;
				$offer['description']		=		"Outside DC, MD, & VA: 2 Years (24 issues) for $69.95";
				$offer['isLocal']			=		false;
				break;
			default: 
				$offer['price']				=		29.95;
				$offer['term']				=		12;
				$offer['description']		=		"DC, MD, & VA: 1 Year (12 issues) for $29.95";
				$offer['isLocal']			=		true;
		}
		return $offer;		
	}
	
	public function generateInternetNumber() {
				// Generating a random number here for internal-only unique ID tracking purposes. 
				// TO DO: Add logic to ensure random number generated isn't already in use. Just in case.
				// Not to be confused with ACCOUNT NUMBER which uniquely identifies the subscriber 
				$random_number 				= 		mt_rand(100000,999999); 
				return $random_number;
	}
	
	public function getPromotionKey() {
		// Defunct function -- promotion key values are now set in _includes/config.php file and are dependent on the form being used 
		/*
		$promotion_key						=		"I13GR";
		return $promotion_key;
		*/
	}
		
	public function collectOrderFormData($data) 
	{
		// Collecting all variables from form into an array. 
		$order['promotionKey']				=		$data['promotionkey'];
		//$order['promotionKey']				=		$this->getPromotionKey();
		$order['firstname']					= 		$data['firstname'];
		$order['lastname']					=		$data['lastname'];
		$order['address1']					=		$data['address1'];
		$order['address2']					=		$data['address2'];
		$order['city']						=		$data['city'];
		$order['state']						=		$data['state'];
		$order['zipcode']					=		$data['zip'];
		$order['country']					=		$data['country'];
		$order['email']						=		$data['email'];
		$order['payment_type']				=		$data['payment_method'];
		$order['payment_cc']				=		$data['payment_cc'];
		$order['payment_exp']				=		$data['payment_exp_month'] . $data['payment_exp_year'];
		$order['value']						=		$data['value'];
		$order['subscription_term']			=		$data['subscription_term'];
		$order['subscribe_offer']			=		$data['subscribe_offer'];
		$order['internetNumber']			=		$this->generateInternetNumber();
		
		$order['has_diff_recipient']		=		$data['mailing_diff_recipient'];
		
		// Get offer information
		$offer_selected						=		$this->getOffer($order['subscribe_offer']);
		$order['subscription_term']			=		$offer_selected['term'];
		$order['value']						=		$offer_selected['price'];
	
		$xml_string							=		"<order><internetNumber>".$order['internetNumber']."</internetNumber><promotionKey>".$order['promotionKey']."</promotionKey><customer><firstName>".$order['firstname']."</firstName><lastName>".$order['lastname']."</lastName><address1>".$order['address1']."</address1><address2>".$order['address2']."</address2><city>".$order['city']."</city><state>".$order['state']."</state><zipCode>".$order['zipcode']."</zipCode><country>".$order['country']."</country><email>".$order['email']."</email></customer><orderItem><value>".$order['value']."</value><subscriptionTerm>".$order['subscription_term']."</subscriptionTerm></orderItem><payment><paymentType>CreditCard</paymentType><creditCardType>".$order['payment_type']."</creditCardType><creditCardNumber>".$order['payment_cc']."</creditCardNumber><creditCardExpire>".$order['payment_exp']."</creditCardExpire>";
		
		if ($order['has_diff_recipient'] == 1) { 
			// THIS IS A GIFT ORDER OR THE MAILING ADDRESS DIFFERS FROM THE BILLING ADDRESS.
			// You need more people. We don't believe you. 
			$order['recipient_first']		=		$data['mailing_firstname'];
			$order['recipient_address1']	=		$data['mailing_address1'];
			$order['recipient_address2']	=		$data['mailing_address2'];
			$order['recipient_city']		=		$data['mailing_city'];
			$order['recipient_state']		=		$data['mailing_state'];
			$order['recipient_zip']			=		$data['mailing_zip'];
			$order['recipient_country']		=		"US";
			
			$xml_string_recipient_addon		=		"<recipient><firstName>".$order['recipient_first']."</firstName>
<address1>".$order['recipient_address1']."</address1><address2>".$order['recipient_address2']."</address2><city>".$order['recipient_city']."</city><state>".$order['recipient_state']."</state><zipCode>".$order['recipient_zip']."</zipCode><country>".$order['recipient_country']."</country><phoneNumber></phoneNumber></recipient>";
			$xml_string						.=		$xml_string_recipient_addon;		
		}
		
		$xml_string							.=		"</payment></order>";
		
		//print "<br/><br/> Here's the XML request being sent to CDS for troubleshooting purpose: <br/><br/>" . $xml_string; 
		
		return $xml_string;

	}
	
	// Returning CDS Response
	public function submitOrder($xml_string) 
	{		
		//global $xml_init_URL;
		//$xml_init_URL						= 	$form_posturl.'?appId='.$form_appid.'&pwd='.$form_pwd;
		$xml_init_URL						=	'https://service.mycdsglobal.com/ws/service/order/WSH?appId=com.washingtonian.washingtonianmagazine.web&pwd=3fe7c7664c8be3c9';
		$xml_builder 						=	'<?xml version="1.0" encoding="utf-8"?>' . $xml_string;
		$ch 								= 	curl_init($xml_init_URL);
		
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_builder);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		
		/* Initial XML Response / CURL RESULT         */
		/* ****************************************** */
		$ch_result = curl_exec($ch);
		
		curl_close($ch);

		/* Convert XML response into JSON, into ARRAY  */
		/* ******************************************* */
		$simpleXml = simplexml_load_string($ch_result);
		$json = json_encode($simpleXml);
		$json = json_decode($json);
		
		return $json;
		
	}
	
	public function getTransactionDetails($api_result) 
	{
		$trans_info['isSuccess'] 			=	$api_result->isSuccess; 
		$trans_info['sentToOffline']		=	$api_result->sentToOffline;
		$trans_info['message']				=	$api_result->message;
		
		return $trans_info;
	}
	
	public function getOrderDetails($api_result)
	{
		global $xml_string;
		$order_info							=	$api_result->order;
		$order_info->xml_string_sent		=	$xml_string;
		return $order_info;
	}
	
	public function logTransaction($trans_info)
	{
		// Defunct function -- may have a use for it later, or something.
		return true;
	}
	
	public function checkEmailforExisting($email) 
	{
		// Check whether email supplied with new order exists in aMember database, and if so, do something about it.
		return true;
	}
	
	public function sendEmailAlert() {
		
		global 		$adminEmail;
		
		$to 								=		$adminEmail;
		$subject 							= 		"Notification: We Have a New Subscriber!";
		$txt 								= 		"Whoooooo hoooooooooooo!";
		$headers 							= 		"From: mbender@washingtonian.com";
		
		mail($to,$subject,$txt,$headers);
		return true;
		
	}
	public function insertOrderDetails($order_info, $trans_info) 
	{
		// Prepare data to be submitted to database. 
		$order								=		$order_info; 
		$transaction						=		$trans_info;
		
		$cds_returned_order					=		json_encode($order);
		
		$data['sub_id']						=		""; //AUTO increment primary key, no value needed

		$data['sub_promotionkey']			=		$order->promotionKey;
		$data['sub_internet_no']			=		$order->internetNumber;
		$data['sub_firstname']				=		$order->customer->firstName;	
		$data['sub_lastname']				=		$order->customer->lastName;
		$data['sub_address1']				=		$order->customer->address1;
		$data['sub_address2']				=		$order->customer->address2;
		$data['sub_city']					=		$order->customer->city;
		$data['sub_state']					=		$order->customer->state;
		$data['sub_zipcode']				=		$order->customer->zipCode;
		$data['sub_country']				=		$order->customer->country;
		$data['sub_email']					=		$order->customer->email;
		$data['sub_payment_type']			=		$order->payment->creditCardType;
		$data['sub_payment_cc']				=		$order->payment->creditCardNumber;
		$data['sub_payment_exp_mo']			=		$order->payment->creditCardExpireMonth;
		$data['sub_payment_exp_yr']			=		$order->payment->creditCardExpireYear;
		$data['sub_subscription_value']		=		$order->orderItem->value;
		$data['sub_subscription_term']		=		$order->orderItem->subscriptionTerm;
		$data['sub_subscription_offer']		=		$order->orderItem->orderTypeCode;
		
		//$data['sub_cds_accountnumber']		=		$order->orderItem->recipient->accountNumber;
		if (empty($order->customer->accountNumber)) {
			$data['sub_cds_accountnumber'] 	= 		'NA';
		}
		else {
			$data['sub_cds_accountnumber']	=	    $order->customer->accountNumber;
		}

		$data['sub_cds_isTransSuccess']		=		$transaction['isSuccess'];
		$data['sub_cds_isSentOffline']		=		$transaction['sentToOffline'];
		$data['sub_cds_msg']				=		$transaction['message'];
		$data['sub_date_submitted']			=		time();
		$data['sub_xml_string_sent']		=		$order->xml_string_sent;
		$data['sub_xml_string_returned']	=		$cds_returned_order;
		
		$email 								=		$data['sub_email'];
		$firstname							=		$data['sub_firstname'];
		$lastname							=		$data['sub_lastname'];
		$address1							=		$data['sub_address1'];
		$address2							=		$data['sub_address2'];
		$city								=		$data['sub_city'];
		$state								=		$data['sub_state'];
		$zipcode							=		$data['sub_zipcode'];
		$country							=		$data['sub_country'];
		$value								=		$data['sub_subscription_value'];
	
		try {
    		$con = new MyDB();
    		//$query = $con -> insertInto("subscribes", $fields = array("sub_id" => "", "sub_promotion"));
    		$query = $con -> insertInto("subscribes", $fields = $data);
    		
    		$compose_redirect				=		"http://www.washingtonian.com/magazine/subscribe/control/confirmation.php?email=".urlencode($email)."&firstname=".urlencode($firstname)."&lastname=".urlencode($lastname)."&address1=".urlencode($address1)."&address2=".urlencode($address2)."&city=".urlencode($city)."&state=".urlencode($state)."&zipcode=".urlencode($zipcode)."&country=".urlencode($country)."&value=".urlencode($value);
    		$this->sendEmailAlert();
			header('Location: '.$compose_redirect);
			print '<META http-equiv="refresh" content="0;URL='.$compose_redirect.'">'; //PHP HEADER redirect not working; to debug
    		return true;
    	}
		catch (Exception $e) {
		    echo 'There was an error: ',  $e->getMessage(), "\n";
		}	


	}
}
?>