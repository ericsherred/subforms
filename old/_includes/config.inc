<?php 

// Sets the location of the form action script. Since this is referenced across multiple forms. 
$form_action_location				=	'../_form_processing.php';

// Promotion Keys -- obtain these from Sara Fox in Circulation
// These keys should be updated monthly. 
// Last updated: September 2013

$promotionKey['control']['a']		=	'I14QSP1';
$promotionKey['control']['b']		=	'I14QSP2';
$promotionKey['special']['a']		=	'I14QSP3';
$promotionKey['special']['b']		=	'I14QSP4';

$adminEmail							=	'mbender@washingtonian.com';

?>