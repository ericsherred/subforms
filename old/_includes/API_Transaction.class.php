<?php 
class API_Transaction 
{
	
	public function getTransactionDetails($api_result) 
	{
		$trans_info['isSuccess'] 			=	$api_result->isSuccess; 
		$trans_info['sentToOffline']		=	$api_result->sentToOffline;
		$trans_info['message']				=	$api_result->message;
		return $trans_info;
	}
	
	public function getOrderDetails($api_result)
	{
		$order_info							=	$api_result->order;
		return $order_info;
	}
	
	public function logTransaction($trans_info)
	{
		// We'll do some processing here, eventually.
		return true;
	}
	
	public function checkEmailforExisting($email) 
	{
		// Check whether email supplied with new order exists in aMember database, and if so, do something about it.
		return true;
	}
	public function insertOrderDetails($order_info) 
	{
		// Database handling of new order details
		return true;
	}
}
?>