<?php 

	session_name('rep_admin');
	session_start();
	
	unset($_SESSION['login']);
	unset($_SESSION['activity']);
	session_unset();
	session_destroy();
	header("location:index.php?session=logout");
 ?>