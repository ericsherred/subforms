<?php
	include('inc/checkHomeSession.inc');

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Admin Login</title>
		<meta charset=utf-8>
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="css/styles.css">
	</head>
	<body>

		<div class="container">

			<h1 class="text-center">Washingtonian Subscription Admin</h1>

	    	<form method="POST" action="inc/process/login.php" class="form-signin" role="form">
				
				<?php 

					if (isset($query)) {
						switch ($query) {
							case 'expired':
								echo '<div class="alert alert-warning">Your session has expired. Please sign in again.</div>';
								break;
							case 'fail':
								echo '<div class="alert alert-warning">The email address and/or password you entered does not match our records.</div>';
								break;
							case 'false':
								echo '<div class="alert alert-danger">Access denied. Please sign in.</div>';
								break;
							case 'logout':
								echo '<div class="alert alert-success">You have successfuly logged out.</div>';
								break;	
							default:
								echo '<div class="alert alert-info">Please sign in.</div>';
								break;
						}
					}
				?>

	        	<!-- <h2 class="form-signin-heading">Please sign in</h2> -->
	        	<input type="text" class="form-control" placeholder="Username" name="user" required autofocus>
	        	<input type="password" class="form-control" placeholder="Password" name="pass" required>
				<!--<label class="checkbox">
	        		<input type="checkbox" value="remember-me"> Remember me
	        	</label> -->
	        	<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
	      	</form>

    	</div> <!-- /container -->

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	</body>
</html>