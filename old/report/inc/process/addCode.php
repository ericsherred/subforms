<?php 

	include('../class.MyDB.inc');
	
	$data = $_POST;

	$fields = array(
		'code_name' => $data['code'],
		'code_desc' => $data['desc'],
		'code_discount' => $data['disc'],
		'code_active' => 1
	);

	try {
		$con = new MyDB();
		$query = $con->insertInto('sub_promo_codes', $fields);
	} catch (Exception $e) {
		echo "<h1>There was a DB error</h1>";
	}

	header('Location:codes.php');
 ?>