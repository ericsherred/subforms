<?php 

	include('../class.MyDB.inc');
	
	$data = $_POST;

	try {
		$con = new MyDB();
	} catch (Exception $e) {
		echo "<h1>There was a DB error</h1>";
	}

	if ($data['deactivate'] != '' && $data['deactivate'] != NULL) {
		$d_id = $data['deactivate'];
		$fields = array('code_active' => 0);
		for ($i=0; $i < count($d_id); $i++) { 
			$where = array('code_id' => $d_id[$i]);
			$query = $con -> updateTable("sub_promo_codes", $fields, $where, $like = false);
		}
	}
	if ($data['activate'] != '' && $data['activate'] != NULL) {
		$a_id = $data['activate'];
		$fields = array('code_active' => 1);
		for ($j=0; $j < count($a_id); $j++) { 
			$where = array('code_id' => $a_id[$j]);
			$query = $con -> updateTable("sub_promo_codes", $fields, $where, $like = false);
		}
		
	}

	header('location:codes.php');

 ?>