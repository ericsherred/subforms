<?php 

	session_name('rep_admin');
	session_start();
	
	if(!isset($_SESSION['login'])) {
		unset($_SESSION['login']);
		unset($_SESSION['activity']);
		session_unset();
		session_destroy();
		header("location:index.php?session=false");
	}
	elseif ($_SESSION['login'] != true) {
		unset($_SESSION['login']);
		unset($_SESSION['activity']);
		session_unset();
		session_destroy();
		header("location:index.php?session=false");
	}
	elseif (!isset($_SESSION['activity'])) {
		unset($_SESSION['login']);
		unset($_SESSION['activity']);
		session_unset();
		session_destroy();
		header("location:index.php?session=false");
	}
	elseif (time() - $_SESSION['activity'] > 900) {
		unset($_SESSION['login']);
		unset($_SESSION['activity']);
		session_unset();
		session_destroy();
		header("location:index.php?session=expired");
	}
	else {
		$_SESSION['activity'] = time();
	}