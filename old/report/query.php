<?php

	include('inc/class.MyDB.inc');
	include('inc/checkSession.inc');
	
	if (isset($_POST) && $_POST != NULL) {
		$data = $_POST;
	
		try {
			$con = new MyDB();
			// $query = $con -> selectFrom("subs_test", $columns = array('sub_id','sub_promotionkey','sub_firstname'), $where = null, $like = false, $orderby = "sub_id", $direction = "DESC", $limit = null, $offset = null);
			$columns = array(
				'sub_id',
				'sub_promotionkey',
				'sub_firstname',
				'sub_lastname',
				'sub_cds_accountnumber',
				'sub_transaction_orders',
				'sub_subscription_value',
				'sub_type',
				'sub_date_submitted',
				'sub_cds_msg',
				// 'sub_address1',
				// 'sub_address2',
				'sub_city',
				'sub_state',
				// 'sub_zipcode',
				'sub_email',
				'sub_promo_code'
			);

			function checkWhere($data) {
				if ($data) {
					return '1';
				}
				else {
					return '0';
				}
			}

			$whereVal = checkWhere($data['key']);
			$whereVal .= checkWhere($data['email']);
			$whereVal .= checkWhere($data['online']);

			switch ($whereVal) {
				case '111':
					$where = array(
						'sub_promotionkey' => $data['key'],
						'sub_email' => $data['email'],
						'sub_cds_isSentOffline' => '',
						'sub_cds_isTransSuccess' => 'true'
					);
					break;
				case '110':
					$where = array(
						'sub_promotionkey' => $data['key'],
						'sub_email' => $data['email']
					);
					break;
				case '101':
					$where = array(
						'sub_promotionkey' => $data['key'],
						'sub_cds_isSentOffline' => '',
						'sub_cds_isTransSuccess' => 'true'
					);
					break;
				case '011':
					$where = array(
						'sub_email' => $data['email'],
						'sub_cds_isSentOffline' => '',
						'sub_cds_isTransSuccess' => 'true'
					);
					break;
				case '100':
					$where = array(
						'sub_promotionkey' => $data['key']
					);
					break;
				case '010':
					$where = array(
						'sub_email' => $data['email']
					);
					break;
				case '001':
					$where = array(
						'sub_cds_isSentOffline' => '',
						'sub_cds_isTransSuccess' => 'true'
					);
					break;			
				default:
					$where = null;
					break;
			}
			
			if ($data['limit'] == 0) {
				$limit = null;
			}
			else {
				$limit = $data['limit'];
			}

			$query = $con->selectFrom($table = "subscribes", $columns, $where, $like = false, $orderby = "sub_id", $direction = "DESC", $limit, $offset = null);
			// var_dump($query);
		} catch (Exception $e) {
			echo "<h1>There was a DB error</h1>";
		}

	}
	else {
		$data['key'] = NULL;
		$data['email'] = NULL;
		$data['online'] = NULL;
		$data['limit'] = 50;
	}

 ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Results - Search Subscriber Database</title>
		<meta charset=utf-8>
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" media="screen">
	</head>
	<body>
		<?php include('templates/nav.php'); ?>
		<div class="container">
			<form action="query.php" method="POST" class="form-horizontal" role="form">
				<a data-toggle="collapse" href="#search"><legend>Search Subscriber Database +</legend></a>
				<div class="form-group collapse <?php echo !isset($query) ? 'in' : '' ?>" id="search">
					<label for="inputKey" class="col-sm-1 control-label">Key:</label>
					<div class="col-sm-2">
						<input type="text" name="key" <?php if ($data['key']): ?>value="<?php echo $data['key'] ?>"<?php endif; ?> id="inputKey" class="form-control" title="PromoKey">
					</div>
					<label for="inputEmail" class="col-sm-1 control-label">Email:</label>
					<div class="col-sm-2">
						<input type="text" name="email" <?php if ($data['email']): ?>value="<?php echo $data['email'] ?>"<?php endif; ?> id="inputEmail" class="form-control" title="Email">
					</div>
					<div class="checkbox col-sm-2">
						<label class="pull-right">
							<input type="hidden" name="online" value="0"><input type="checkbox" name="online" <?php if ($data['online']): ?>checked<?php endif; ?> value="1">
							Only Processed
						</label>
					</div>
					<label for="inputKey" class="col-sm-1 control-label">Limit:</label>
					<div class="select col-sm-1">
						<select name="limit" id="inputLimit" class="form-control">
							<option value="25" <?php echo $data['limit'] == 25 ? 'selected' : '' ?>>25</option>
							<option value="50" <?php echo $data['limit'] == 50 ? 'selected' : '' ?>>50</option>
							<option value="100" <?php echo $data['limit'] == 100 ? 'selected' : '' ?>>100</option>
							<option value="0" <?php echo $data['limit'] == 0 ? 'selected' : '' ?>>All</option>

						</select>
					</div>
					<div class="col-sm-2">
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</form>

			<?php if(isset($query)): ?>
				<h2><?php echo $query['num']; ?> results returned</h2>
			<?php endif; ?>

			<div class="table-responsive">
				<table class="table table-hover table-striped table-condensed">
					<thead>
						<tr>
							<th>ID</th>
							<th>PromoKey</th>
							<th>First</th>
							<th>Last</th>
							<!-- <th>Accont</th> -->
							<th>Ordered</th>
							<th>Cost</th>
							<th>Type</th>
							<th>Date</th>
							<th>City</th>
							<th>State</th>
							<th>Promo Code</th>
							<th>CDS Message</th>
						</tr>
					</thead>
					<tbody>
						<?php if(isset($query)): ?>
						<?php 
							for ($i=0; $i < $query['num']; $i++) { 
								echo '<tr>';
									echo '<td>';
										echo $query['result'][$i]['sub_id'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['sub_promotionkey'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['sub_firstname'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['sub_lastname'];
									echo '</td>';
									/*echo '<td>';
										echo $query['result'][$i]['sub_cds_accountnumber'];
									echo '</td>';*/
									echo '<td>';
										echo $query['result'][$i]['sub_transaction_orders'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['sub_subscription_value'];
									echo '</td>';
									echo '<td>';
										if ($query['result'][$i]['sub_type'] == 'C') {
											echo 'Print';
										}
										elseif ($query['result'][$i]['sub_type'] == 'I') {
											echo 'Digital';
										}
										else {
											echo 'Unknown';
										}
									echo '</td>';
									echo '<td>';
										echo date('M d Y H:i',$query['result'][$i]['sub_date_submitted']);
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['sub_city'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['sub_state'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['sub_promo_code'];
									echo '</td>';
									echo '<td>';
										echo $query['result'][$i]['sub_cds_msg'];
									echo '</td>';
								echo '</tr>';
							}
						?>
					<?php endif; ?>
					</tbody>
				</table>
			</div>
			<div>
				<?php if (isset($query)): ?>
					<pre><?php echo $query['sql'] ?></pre>
				<?php endif; ?>
			</div>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
	</body>
</html>