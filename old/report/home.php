<?php 
	include('inc/checkSession.inc');
 ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Washingtonian Subscriptions Admin</title>
		<meta charset="UTF-8">
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" media="screen">
	</head>
	<body>
		<?php include('templates/nav.php'); ?>

		<div class="container">
			<h1 class="text-center">Welcome To The Admin Panel</h1>

			<div class="list-group">
				<a href="query.php" class="list-group-item">
					<h4 class="list-group-item-heading">Search Subscribers Database</h4>
					<p class="list-group-item-text">Useing PromoKey, email and successful transactions to search the in house Washingtonian Database.</p>
				</a>
			
				<!-- <a href="codes.php" class="list-group-item">
					<h4 class="list-group-item-heading">Search the promotion code database</h4>
					<p class="list-group-item-text">Find, add and deactivate subscription promotion codes.</p>
				</a> -->
			
				<!-- <a href="key.php" class="list-group-item">
					<h4 class="list-group-item-heading">Search Promo Keys</h4>
					<p class="list-group-item-text">Search and add CDS promo keys for active subscription forms.</p>
				</a> -->
			</div>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
	</body>
</html>