<?PHP 

/* 	Subscriber/Order Processing against CDS API								*/
/* 	Author: Mario Starks													*/
/* 	Origin Date: 07/08/2013													*/
/*  V2 Date:	08/07/2013													*/
/* 	Description: POST AND GET XML RESPONSE via CURL 						*/
/* 	STEP 1: GET API RESULT 													*/
/* 	STEP 2: GET TRANSACTION INFO to verify SUCCESS/FAILURE OF API REQUEST 	*/
/* 	STEP 3: GET ORDER DETAILS based on successfully returned API REQUEST 	*/

include('_includes/database_class.inc');
include('_includes/Order.class.php');   		 /* Handles most operations */

/* FOR TESTING PURPOSE: If no form submission, passing along an empty $_POST value */
if (empty($_POST))
{
	$_POST['empty'] 						= 	'';
	$data									=	$_POST;
}
else { 
	$data 									=	$_POST;
}
/* END FOR TESTING PURPOSE */

$order 										=	new Order;

try {

	$xml_string								=	$order->collectOrderFormData($data); 						/* Assembles all user form input into a clean XM string value 			*/
	$api_result								=	$order->submitOrder($xml_string); 							/* Takes XML response from CDS and converts into an array of values 	*/
	
	if (!empty($api_result)) {
		$trans_info							=	$order->getTransactionDetails($api_result);					/* Checks whether API received new order as successful or with error 	*/ 
		$order_info							=	$order->getOrderDetails($api_result);						/* Separating order specitic information, such as accnt number 			*/
		//$order_info['xml_string_sent']		=	$xml_string;
	}	
	else {
		print "Unable to access API at this time to submit order.";											/* If this message shows, the CDS API/server is likely inaccessible.	*/ 
	}
	
	$isLogged								=	$order->logTransaction($trans_info);						/* Logs transaction internally 											*/
	$isOrderInserted						=	$order->insertOrderDetails($order_info, $trans_info);		/* Records successful order information into our internal DB			*/
	
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}

if ($isOrderInserted) {
	// Redirect or any other operations we want to do after successfully adding a new subscriber to the database 
	$emailSent								=	$order->sendEmailAlert();
}

/* DO NOT REMOVE : HELPER CODE: For Development / Troubleshooting -- returns values of app-required array data

print "<pre>";
print_r($api_result);
print_r($trans_info);
print_r($order_info);
print "</pre>";

*/

?>