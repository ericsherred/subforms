<?php 

/* 	SUBSCRIBE FORMS REDIRECT														*/
/* 	A/B Testing Using Very Basic Random Redirect Logic								*/
/* 	Success tracking done via Google Analytics										*/

/* 	To add another form option, add the URL to the 
	$subscribe_forms array as in the lines below 									*/
	
$subscribe_forms[]		=	'https://www.washingtonian.com/magazine/subscribe/control/subscribe-control-a.php';
$subscribe_forms[]		=	'https://www.washingtonian.com/magazine/subscribe/control/subscribe-control-b.php';
$subscribe_forms[]		=	'https://www.washingtonian.com/magazine/subscribe/special/subscribe-special-a.php';
$subscribe_forms[]		=	'https://www.washingtonian.com/magazine/subscribe/special/subscribe-special-b.php';

$formToServe			=	array_rand($subscribe_forms);

header('Location: ' . $subscribe_forms[$formToServe]);

?>