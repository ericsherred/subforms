<?php 
    $email		=	$_GET['email'];
    $firstname	=	$_GET['firstname'];
    $lastname   =	$_GET['lastname'];
    $address1	=	$_GET['address1'];
    $address2	=	$_GET['address2'];
    $city		=	$_GET['city'];
    $state		=	$_GET['state'];
    $zipcode	=	$_GET['zipcode'];
    $country	=	$_GET['country'];
    $value		=	$_GET['value'];
    $date		=	date_create();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Thank You | Washingtonian</title>
		<meta charset="UTF-8">
		<meta name=description content="">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" media="screen">
		<link rel="stylesheet" href="css/responsive.css">
		<link rel="stylesheet" href="css/thanks.css">
	</head>
	<body>
		<div class="container">
			<div class="body">
				<div class="sub hidden-xs hidden-print">MAGAZINE SUBSCRIPTION FORM</div>
				<div class="clearfix">
					<div class="col-sm-12 col-md-9">
						<div class="row head text-center"><a href="http://www.washingtonian.com"><img src="images/Wash-Logo-Blue.png" alt="Washingtonian Magazine" height="66"></a></div>
						<div class="col-xs-12 thanks">
							<div class="col-sm-4 col-lg-3 hidden-xs hidden-print"><img src="http://www.washingtonian.com/articles/images/2014_Feb_small.jpg" alt="latest cover" class="img-thumbnail" /></div>
<<<<<<< HEAD
							<div class="col-sm-8 col-lg-9"><h1 class="thanks"><em>Thank you from Washingtonian Magazine</em></h1>
							<h3 class="thanks">Your source for dining, nightlife, news, shopping and more in the Washington region.</h3></div>
=======
							<div class="col-sm-8 col-lg-9"><h1 class="thanks"><em>Thank you from Washingtonian Magazine</em><br><small>Your source for dining, nightlife, news, shopping and more in the Washington region.</small></h1></div>
>>>>>>> 6d66207af8846206666834e78d7eccc92400aedc
						</div>
						<div class="col-xs-12">
							<div class="col-lg-6">
								<h2>Subscription Confirmation</h2>
<<<<<<< HEAD
								<p>Thank you for becoming a subscriber to Washingtonian.</p>
=======
								<p>Thank you for becoming a digital subscriber to Washingtonian.</p>
>>>>>>> 6d66207af8846206666834e78d7eccc92400aedc
								<p>To log into your iPad edition, please <a href="http://www.washingtonian.com/ipad-app" target="_blank">click here</a>.
								<p><strong>Usvername:</strong> <?php echo $email; ?><br><strong>Password:</strong> <?php echo $zipcode; ?></p>
								<p>If you have any questions, do not hesitate to contact us.</p>
								<p><strong>202-331-0715</strong>
									<br>Monday - Friday 9a - 5p EST
									<br><strong>800-365-7050</strong>
									<br>Monday - Friday 8a-10:30p, Saturday 9a-7p EST
									<br><a href="mailto:washsub@washingtonian.com">washsub@washingtonian.com</a></p>
							</div>
							<div class="col-lg-6">
							<h2>Billing Information</h2>
								<p>
									<?php echo $firstname, ' ', $lastname, '<br>'; ?>
									<?php echo $address1, '<br>'; ?>
									<?php echo ($address2 != '')? $address2.'<br>' : '' ; ?>
									<?php echo $city, ', ', $state, ' ', $zipcode, '<br>'; ?>
									<?php echo $email ?>
								</p>
								<p><?php echo 'Total Charged: $', $value; ?></p>
							</div>
						</div>
						<div class="col-xs-12 text-center">
							<a href="javascript:window.print()" class="btn btn-lg btn-default hidden-print"><i class="glyphicon glyphicon-print"></i> Print</a> <a href="http://www.washingtonian.com" class="btn btn-lg btn-primary hidden-print">Return to Washingtonian.com</a>
						</div>
					</div>
					<div class="col-md-3 side">&nbsp;</div>
				</div>
			</div>
			<div class="footer">
				<img class="hidden-xs" src="images/Wash-Logo-Blue.png" height="20" />
 				<p class="small" style="margin-top:5px;">&copy;2013 Washington Magazine Company. 1828 L St. NW #200, Washington, DC 20036</p>
 				<p class="small" style="margin-top:8px;"><a style="color:#085899;" href="http://www.washingtonian.com/privacy-policy/">Privacy Policy</a> &nbsp; | &nbsp; <a style="color:#085899;" href="http://www.washingtonian.com/contact-us/">Contact Us</a></p>
			</div>
		</div>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
	</body>
</html>