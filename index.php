<?php 

/* 	SUBSCRIBE FORMS REDIRECT														*/
/* 	A/B Testing Using Very Basic Random Redirect Logic								*/
/* 	Success tracking done via Google Analytics										*/

/* 	To add another form option, add the URL to the 
	$subscribe_forms array as in the lines below 									*/
	
$subscribe_forms[]		=	'forms/test-a.php';
$subscribe_forms[]		=	'forms/responsive-a.php';

$formToServe			=	array_rand($subscribe_forms);

header('Location: ' . $subscribe_forms[$formToServe]);

?>